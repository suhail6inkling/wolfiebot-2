import discord
from discord.ext import commands

def GMCheck(ctx):
    if ctx.guild:
        if "Game Master" in [role.name for role in ctx.author.roles]:
            return True
    return False

def GMAltCheck(ctx):
    if ctx.guild:
        if "Game Master" in [role.name for role in ctx.author.roles]:
            return True
        if "GM Alt" in [role.name for role in ctx.author.roles]:
            return True
    return False


GM = commands.check(GMCheck)

GMAlt = commands.check(GMAltCheck)




owner = commands.is_owner()