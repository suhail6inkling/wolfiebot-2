import discord
from cogs.lib import Icons as icons

agent = discord.Embed(description="""*My watch is a nice piece of jewellery. Always loved it. The silver rim reminds me of the scope I used to kill our last mayor. I got paid quite well for that. Second-best assignment I've had since taking down a terrorist threat. I used the money to buy this watch. It wasn't much - my watch is just cheap garbage. Always hated it.*
**Actions:**
*Loyalty* - Every night, while there are two or more other players of their alignment alive (unless the Agent is Neutral), must choose a player (choosing a random player if offline). Gains the objectives and alignment of that player until the start of the next night. May not choose a player whom they have already chosen.
*Heal* - May give one player an Active Standard Save every night. This Save only takes effect if the target is of the same alignment as the Agent. If there are two or less other players of their alignment alive, this Save is Powerful rather than Standard.
**Abilities:**
- If they die, they keep their assumed objective and alignment.
- When investigated, they appear as a random non-unique role of the alignment of the player investigating them.
- If they target Good players three nights in a row with *Loyalty*, they become a Spy.
- If they target Evil players three nights in a row with *Loyalty*, they become a Backstabber.
**Objectives:**
- None
**Tags:**
- Neutral
- Chaos/Protective
- Human""",colour=0x00ff85)
agent.set_thumbnail(url=icons["agent"])

alchemist = discord.Embed(description="""*Descended from a line of practitioners and continuing the work of their forefathers: Alchemy, an ancient art used to transmute - not only metals, but life itself.*
**Actions:**
*Homunculus* - At night, may create a Homunculus. That night, all Direwolves in the game appear to be a random Good Human role when targeted with *Investigate*. This action uses up one of the Alchemist's Potions.
*Chaos Serum* - At night, may target any player. That player's alignment changes from Good to Evil, or vice versa. If that player is Neutral, it changes to Evil. At the start of the second day after this action is used, the target player's alignment changes back to what it was before this action was used, even if their role or alignment has changed through other means since. This action uses up one of the Alchemist's Potions.
*Potion of Judas* - At night, may target any player. That player's role changes to a random role of the same alignment. This action uses up one of the Alchemist's Potions.
*Bewitch* - If targeted by *Maul* or *Pack Offensive*, may choose any player for the action to be redirected towards instead.
**Abilities:**
- Has an amount of potions equal to half the amount of players in the game. Only one potion may be used per night.
- Cannot become a Wolf by any means.
- Counts as Evil for the objectives of Wolves.
**Objectives:**
- Be the last player alive, excluding Wolves.
**Tags:**
- Neutral
- Chaos/Support
- Arcane
- Unique""",colour=0x8c8cff)
alchemist.set_thumbnail(url=icons["alchemist"])

anarchist = discord.Embed(description="""**Actions:**
*Strategically-Implemented Self-Murder* - Once per game, at night, commits Suicide.
*Spanner* - Once per night, target any player. Any actions that player makes that night automatically fail. 
*Persuade* - Twice per game, at night, while not using *Molotov*, select two other players. The two switch roles and alignments until the end of the following night. Cannot target the same player twice with this action. 
*Molotov* - Once every two nights, target any player with a Strong Attack. Cannot be used on the same night as *Strategically-Implemented Self-Murder*.
**Abilities:**
- If killed by a Lynching, becomes a Spectre.
**Objectives:**
- Finish the game with no living players. If the last player alive is the Anarchist, this objective fails. 
**Tags:**
- Neutral
- Chaos/Killing
- Human
- Unique""",colour=0xff9400)
anarchist.set_thumbnail(url=icons["anarchist"])

apothecary = discord.Embed(description="""**Actions:**
*Bane* - Starting DAY 2, once every two days, at any time during the day, before the Lynch vote is over, may choose any living player. It is announced that the Apothecary has used *Bane*. At the same time as lynch, that player is targeted with a Powerful Attack, providing that *Antidote* has not been used. If this attack causes the death of a Good player, the Apothecary cannot use *Bane* again for the rest of the game.
*Antidote* - At any time during the day, after using *Bane*, may choose to prevent *Bane*'s attack from working. This also prevents Lynch from acting on the player targeted by *Bane*, if this action is used before the Lynch vote is over.
**Abilities:**
- Gains a Lunar Strong Save at the start of every night.
- If turned Evil, becomes a Dentist.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Killing/Protective
- Human
- Unique""",colour=0x5dff00)
apothecary.set_thumbnail(url=icons["apothecary"])

archfey = discord.Embed(description="""**Actions:**
*Restore* - Every night, may choose two players. Each player receives an Active Strong Save.
*Fortune* - Every two nights, can choose a player. The player gains one free use of the Pixie's *Flip* action. If the targeted player is a Pixie, they instead gain one penny.
*Sleep* - Once per game, may choose to make all Attacks except those of Unstoppable strength fail that night.
**Abilities:**
- Is given a Lunar Strong Save at the start of each night.
- Is told the result of the penny flip from *Fortune*.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Protective/Support
- Ethereal
- Achievable""",colour=0x5dff00)
archfey.set_thumbnail(url=icons["archfey"])


arsonist = discord.Embed(description="""*A box of matches. A stick of wood. A can of oil. A switch of a lighter. A smirk of victory. A city of flames.*
**Actions:**
*Douse* - Every night, chooses one player to douse in gasoline. Players do not know if they are doused in gasoline.
*Ignite* - Instead of using *Douse*, may ignite all players doused in gasoline. This targets them with an Unstoppable Attack, and they become no longer doused in gasoline. On the night that the Arsonist uses *Ignite*, all players who target them are doused in gasoline and also ignited, however the Arsonist is not saved from any attacks.
**Abilities:**
- Upon death, becomes a Spectre and loses access to *Douse* action.
**Objectives:**
- Be the last player alive, or finish the game with no living players.
**Tags:**
- Neutral
- Chaos/Killing
- Human
- Unique""",colour=0xff9400)
arsonist.set_thumbnail(url=icons["arsonist"])

assassin = discord.Embed(description="""**Actions:**
*Terminate* - Every night, may target any player with a Standard Attack.
*Neutralise* - Three times per game, while not using *Terminate*, may choose a player. That player loses all their Standard and Strong saves.
**Abilities:** 
- At the start of NIGHT 1, a random player, other than the Assassin, becomes the Assassin's Client. The Assassin is told this player's identity and the Client is told that they have become such.
- The Client, during that night, chooses a role. They must choose again if this role is not present in the game. The Assassin is informed of the Client's choice. All players with the role chosen by the Client become Targets.
- If all Targets die by means other than an attack from the Assassin, or the Client is killed, a new Client is chosen and new Targets are set during the following night. 
- After a Target is killed by the Assassin, the Assassin gains a Strong save, and a new Client is chosen who chooses a new role.
**Objectives:**
- Kill an amount of Targets by the end of the game equal to a quarter of the amount of players, rounding down. 
**Tags:**
- Neutral
- Killing
- Human""",colour=0xbcbcbc)
assassin.set_thumbnail(url=icons["assassin"])

backstabber = discord.Embed(description="""*Honour? Dignity? Loyalty? Oh, please, you're a child. I want to live and I'll do it by any means. I'll use you as a tool and the moment you're no longer useful to me, I'll toss you aside and pick another shield. I'll kick you in the balls and you'll smile to my face. I'll use you and abuse you and take every last penny from you until I drive a knife into your neck. And the best part? I will feel **nothing**.*
**Actions:**
*Betray* - Every night, may choose a player. If that player does not have the same alignment as the Backstabber, and has targeted the Backstabber with any action before the night this action is used, they are targeted with a Powerful Attack. If they die from this attack, it is announced that they committed Suicide.
**Abilities:**
- Always appears as a random Good role when targeted with *Investigate*.
- If any action or ability would cause the Backstabber to gain a Modifier or a Save, unless it is the action *Infect*, the player whose action or ability it was is targeted with an attack of Strength one level greater than the Save that would have been given, or Powerful if the Backstabber would have gained a Modifier. If they die from this attack, it is announced that they committed Suicide.
- Cannot begin the game with any Modifiers.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Counteractive/Killing
- Human
- Unique""",colour=0xff2323)
backstabber.set_thumbnail(url=icons["backstabber"])

bard = discord.Embed(description="""*The composer in the concerto of life. Corralling crescendos to be measured and beat, listening to the symphonies of screams, tuned to perfection and ensembled in harmony.*
**Actions:**
*Direct* - Every night, chooses a player. Flip a coin for all players with the Minstrel Modifier, even if they are the chosen player. For one heads, the chosen player is targeted with a Standard Attack; for two, a Strong Attack; for three, a Powerful attack and for four or more an Unstoppable Attack. The Bard does not count as targeting the chosen player but all Minstrel players do. Players with the Minstrel Modifier are told which player was targeted but not the results of any coin flips.
*Initiate* - Once per game, is told when no players with the Minstrel Modifier remain, and chooses one player to apply the Modifier to.
**Abilities:**
- The Minstrel Modifier is only present in games where the Bard appears, and is guaranteed to be applied to at least two players (the Bard may not receive this Modifier). 
- The Bard does not know which players have the Minstrel Modifier and those with the Modifier do not know the identity of the Bard.
**Objectives:**
- Be the last player alive. May spare up to two players providing they have the Minstrel Modifier.
**Tags:**
- Neutral
- Chaos/Killing
- Human
- Faction: *Troupe*
- Unique""",colour=0xff0098)
bard.set_thumbnail(url=icons["bard"])

baykok = discord.Embed(description="""**Actions:**
*Spectral Arrows* - Every night, may choose any player, and a role. The player is targeted with a Standard Attack. If that player's role is Killing, they are targeted with a Powerful Attack instead. If that player is Support, the attack fails. If the player dies from this action and Baykok was the only player to target them with an Attack, they appear to have been killed by the role chosen by the Baykok.
*Harvest* - Once per game, at night, may choose any player. If that player has any limited-use actions, they lose all uses of them.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Counteractive/Killing
- Ethereal
- Unique""",colour=0xff2323)
baykok.set_thumbnail(url=icons["baykok"])

bloodhound = discord.Embed(description="""**Actions:**
*Maul* - Discuss with the other wolves in a private channel who to Strong Attack during the Night
*Pack Offensive* - Once per game, at night, not before NIGHT 3, instead of using Maul, the Wolves may choose to each individually target any player with a Standard Attack
*Fang* - Every two nights, all Vampires together may target any player. That player loses all of their saves. If that player had no saves, they become a Vampire. If the player targeted had also been targeted with Infect that night, was a Werewolf or had the Feral modifier, they become a Bloodhound rather than a Vampire.
*Unholy Tribute* - Once per game, may select any Wolf or Vampire and any other player. Both chosen players are targeted with an Unstoppable Attack.
**Abilities:**
- All Vampires and Bloodhounds in the game may speak in a collective private channel with one another. They may still speak in this channel after death.
- Whenever any player (other than one who was already a Vampire) becomes a Vampire or a Bloodhound, all Vampires and Bloodhounds other than the that player gain a Queued Standard Save.
- If targeted by any effect that would cause an alignment change without changing the Vampire's role, commits Suicide.
- If there are no Vampires alive and no Wolves other than the Bloodhound left alive, the Bloodhound gains a Queued Strong Save.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Killing/Support
- Wolf
- Factions: *Vampiric, Wolves*
- Unique
- Achievable""",colour=0x9b0029)
bloodhound.set_thumbnail(url=icons["bloodhound"])

clockmaker = discord.Embed(description="""*A thousand rusted, ancient cogs turning around in the belly of the beast. Dwelling inside your head, the infernal noise. Tick tock. Tick tock. Tick tock. With every second that passes, you take one step closer to your own demise.*
**Actions:**
*Pendulum* - Every night, can choose one player to target with a Standard Attack. If the targeted player dies and is Good, the Clockmaker's clock moves forwards one, if they are Evil, the clock moves backwards one, if they are Neutral, the clock moves forward three.
**Abilities:**
- Has a clock which starts set to 8.
- If their clock strikes 6, they commit Suicide.
- If their clock strikes 10, they gain a Queued Strong Save. This happens each time the clock strikes 10.
- If their clock strikes 11, Pendulum is a Powerful Attack rather than a Standard Attack.
- Can only appear in a game with one other Neutral player.
- If targeted with *Invite*, becomes a Tardis Engineer.
- Cannot change alignment or objective by any means, unless they also change role. If an effect would cause the Clockmaker to change their alignment or objective without also changing their role, it fails.
**Objectives:**
- Have their clock strike 12.
**Tags:**
- Neutral
- Killing
- Human""",colour=0xff9400)
clockmaker.set_thumbnail(url=icons["clockmaker"])

companion = discord.Embed(description="""*When I saw the gleam in this child's eye, I knew I had to welcome them onboard. But I have to be careful, I can't let them go like the others... banished to another dimension... forced to forget... ripped apart by time itself... but not them. Never them.*
**Abilities:**
- See Time Lord (‘*w.roles_timelord*’).
- If targeted with *Invite*, becomes a TARDIS Engineer.
**Objectives:**
- Identical to their Time Lord.
**Tags:**
- Identical alignment to their Time Lord.
- Faction: *Tardis*
- Modifier
- Achievable""",colour=0x204eff)
companion.set_thumbnail(url=icons["companion"])

cultist = discord.Embed(description="""*As your promulgator, I bid you, say it with me- Brothers! Sisters! The great devotion shall not be swayed, we shall be venerated! We shall see the rise of our one true saviour and live to see the day this world is cured of its sickness!*
**Actions:**
*Convert* - Once per game, can choose one player to convert to Evil. In the event that this player is already Evil, the Cultist may not try again.
*Sacrifice* - Once per game, at night, can choose two players, one of whom is already dead. The dead player is revived as a random non-Unique Evil role, and the living player commits Suicide.
*Curse* - Once per game, can remove all Lunar and Active Saves for Good roles.
**Abilities:**
- The presence of the Cultist is revealed at the start of the game, but not their identity.
- If the Cultist is present, the Priest must also be present.
- If any Priest becomes a Paladin, becomes a Warlock. 
- If alignment is changed to Good, becomes a Priest.
- If any Priest becomes a Cultist, becomes a Priest.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Counteractive/Support
- Human
- Unique""",colour=0xff2323)
cultist.set_thumbnail(url=icons["cultist"])

cyberhound = discord.Embed(description="""**Actions:**
*Maul* - Discuss with other Wolves in a private channel who to Strong Attack during the night.
*Pack Offensive* - Once per game, at night, not before NIGHT 3, instead of using *Maul*, the Wolves may choose to each individually target any player with a Standard Attack.
*Glitch* - Every night, chooses two players and chooses a non-Unique role for each player. If either player dies during that night or the following day and their role has not been changed since, they appear to be the role chosen for them and the Cyberhound is told their actual role. If a player the Cyberhound has chosen during that night is targeted with *Shift*, the player using the action becomes the role chosen by the Cyberhound rather than the player’s actual role. They still gain any modifiers the player had.
**Abilities:**
- On death, the real roles of all the dead players that *Glitch* took effect on are revealed.
- May still speak in the Wolf channel when dead.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Counteractive/Killing
- Wolf
- Faction: *Wolves*
- Unique
- Achievable""",colour=0xff5000)
cyberhound.set_thumbnail(url=icons["cyberhound"])

dentist = discord.Embed(description="""*A recent study suggests 67.34% of children fear going to the dentist. I wonder why. Why would anyone fear having a cold, metallic scalpel digging around in their mouth? Why would anyone fear whizzing drills and powerful machinery burying into the gums of patients? Why would anyone fear having every tooth forcefully extracted from their bleeding jaws as they sing a symphony of roaring agony? C'mon, buddy, we're not all that bad if you get to know us...*
**Actions:**
*Lockjaw* - Every night, select any player. That player may not speak in *#game* or vote in *#voting* the following day. The town is not told about this effect, however the target player is. If the target player is Lynched, it is announced that they were lockjawed.
*Laughing Gas* - Once per game, at night, may select any player. If they were targeted with *Lockjaw* the previous night, that player becomes an Evil Jester.
**Abilities:**
- Is not affected by *Haunt*.
- If turned into Good, becomes an Apothecary.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Counteractive
- Human
- Unique""",colour=0xff2323)
dentist.set_thumbnail(url=icons["dentist"])

direwolf = discord.Embed(description="""*The Direwolf is a beast, yet it doesn't rely on instinct. The Direwolf is a machine, yet it cannot be upgraded. The Direwolf is impossible, yet it is not fictional. The Direwolf is evil, yet it is not fallible. The Direwolf is godly, yet it is not righteous. The Direwolf is coming. The Direwolf is coming for you.*
**Actions:**
*Maul* - Discuss with other Wolves in a private channel who to Strong Attack during the night.
*Pack Offensive* - Once per game, at night, not before NIGHT 3, instead of using *Maul*, the Wolves may choose to each individually target any player with a Standard Attack.
*Infect* - Each night, choose a non-Wolf player. That player gains an Active Unstoppable Save. This Save is always bypassed by *Maul* and *Pack Offensive*. If this Save is used before the start of the next night, that player becomes an Evil Werewolf, or an Evil Bloodhound if they were a Vampire. Until the start of the next night, the chosen player appears to be an Evil Werewolf when targeted with *Investigate*.
**Abilities:**
- May still speak in the Wolf channel when dead.
- Leads the Wolves - while alive, always makes the final choice on the target of *Maul*.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Killing/Support
- Wolf
- Faction: *Wolves*
- Unique""",colour=0xff5000)
direwolf.set_thumbnail(url=icons["direwolf"])

doctor = discord.Embed(description="""*I tried, alright? I tried to be all I could be. I followed my oaths, I learned my ways, I did everything that was asked of me. But I needed the money and, well, can you blame me for it? This title is not one I deserve after what I've done and, in any other circumstance, I wouldn't take it. But these people are hurt, dying and constantly screaming for help. These people need a helping hand, a light through the tunnel, a saviour. These people need a doctor.*
**Actions:**
*Amputate* - May target one player with an amputation every night. If that player is Good, they lose all Queued Saves. If they are Neutral, they are targeted with a Standard Attack. If they are Evil, they are targeted with a Powerful Attack.
*Bandage* - If they don't use *Amputate*, may give one player a Lunar Strong Save instead.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Killing/Protective
- Human""",colour=0x5dff00)
doctor.set_thumbnail(url=icons["doctor"])

dodomeki = discord.Embed(description="""*She seemed to have come out of nowhere, frequenting the temple upon the hill. The unusually long sleeves of her robe were strange, and the frail hands which somehow emerged from the ends never failed to unnerve whoever noticed them. But none knew that under the light of the moon, the robe would come off. Hundreds of eyes, wrapped round thickly veined forearms, would open into the windows of the soul.*
**Actions:**
*Eyes On You* - Every other night, may choose a player. The Dodomeki learns the names of all of the actions that player used that night.
**Abilities:**
- Is told all players who targeted them during the night at the end of the night.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Investigative
- Unearthly
- Unique""",colour=0xff2323)
dodomeki.set_thumbnail(url=icons["dodomeki"])

drunk = discord.Embed(description="""*Ugghhhh... Dang, what happened last night? Where am I? Who am I? Ah, I'm sure it'll come to me eventually...*
**Actions:**
*Swig* - Twice per game, can choose to give themselves a Lunar Powerful Save. If this Save is removed without taking effect, the Drunk's transformation is delayed by a night.
*Overdose* - Once per game, while not using Swig, can target themselves with a Standard Attack. If this attack is saved against, the Drunk's transformation is expedited by a night. If the Save used came from *Infect*, the Drunk becomes a Cyberhound.
**Abilities:**
- At the start of NIGHT 3, a random Achievable role of a particular alignment is chosen. The Drunk becomes that role. The player is decided using the following method: If the Direwolf is alive, then Evil. Otherwise, if the Seer is alive, then Good. If neither are alive, then the Drunk can choose their alignment.
- If they die before the night they transform, they gain the Spectre modifier.
**Objectives:**
- None
**Tags:**
- Neutral
- Chaos
- Human
- Unique""",colour=0xffffff)
drunk.set_thumbnail(url=icons["drunk"])

feral = discord.Embed(description="""**Abilities:**
- Starts the game with a Queued Strong Save.
- For objectives purposes, act as if this player was a Wolf.
- When targeted with *Investigate*, this player appears to be an Evil Werewolf.
**Tags:**
- Modifier""",colour=0xff5000)
feral.set_thumbnail(url=icons["feral"])

# geneticist = discord.Embed(description="""**Actions:**
# *Experiment* - Every night, while not in a Twins chat, can choose any 2 players to become Twins. The Geneticist joins the private channel created for these twins, and leaves it if both twins die. If the twins are required to choose an alignment between them, the Geneticist chooses for them.
# **Abilities:**
# - The Twin modifier cannot be applied to the Geneticist.
# - Is told the identity of all players with the Twin modifier at the start of the game, however not which player they are twinned with.
# - Does not need to be killed to fulfill the objective of any player whom they are in a twins private channel with.
# **Objectives:**
# - Survive until the end of the game.
# **Tags:**
# - Neutral
# - Chaos/Support
# - Human
# - Unique""",colour=0x9900ff)
# geneticist.set_thumbnail(url=icons["geneticist"])

gladiator = discord.Embed(description="""*The clang of steel, the smell of blood and the inescapable roar of the masses. Two enter, one makes it out alive. Only the people can save you now.*
**Actions:**
*Challenge* - Every three nights, may challenge another player. The next day, so long as the Gladiator is still alive, the only players that can be voted to be Lynched are the Gladiator and their target. The Lynching is an Unstoppable Attack rather than a Standard one.
*Pressure* - Once per game, at day, can choose to make the Lynching a Strong Attack. If another action used the previous night affects the strength of the Lynching, this action fails and can’t be used again.
*Resolve* - Every day, can give someone an Active Standard Save. This cannot be given the day after *Challenge* and cannot be given to the same player twice in a row.
**Abilities:**
- If the Gladiator is Mayor or Deputy, they may use *Challenge* every two days instead of every three.
- If a use of *Challenge* results in the death of an Evil role, the Gladiator may use *Challenge* every two days instead of every three.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Counteractive/Killing
- Human
- Unique""",colour=0x5dff00)
gladiator.set_thumbnail(url=icons["gladiator"])

glazier = discord.Embed(description="""*It was his finest creation yet. A delicate coat of glass fixed on the finest layer of silver, a bronze, ornately-carved frame wrapping itself around the smoothed edge, and of course, just a pinch of infused magic.*
**Actions:**
*Reflect* - 3 times per game, at night, can choose to reflect all actions for the night. That night, all actions that target the Glazier are targeted instead at the player who used the action. If the Glazier is targeted by the Wolves' attack, a Wolf is randomly selected to be targeted instead. An exception is made for Suicides; the Glazier still commits Suicide if an effect would cause them to. 
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Counteractive
- Human""",colour=0x5dff00)
glazier.set_thumbnail(url=icons["glazier"])

hacker = discord.Embed(description="""*That's the problem with today's society. Everything's **digital**. Any idiot with a computer and a bit of luck is able to steal thousands from your wallet, without ever having to nab a single coin. I mean, seriously, could you imagine that? Some virgin in his mother's basement could - oh, I don't know - deprogram a direwolf, steal precious intel on his cohorts, tell the entire city every last secret they have in their paws... all with a laptop and a few clicks of a mouse.*
**Actions:**
*Hack* - Every night, may choose another player and flip a coin until they get tails. The Hacker is told that player’s alignment, and for every heads, the target of one of their previous actions, starting from their latest action. If an action has multiple targets, the Hacker is told all targets. The Hacker is not told when these actions were used, just the order in which they were leading up to the present.
*Deprogram* - Once per game, may choose one player. If that player has changed role, alignment or modifiers since the start of the game, they become what they were at the beginning of the game.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
- If a use of *Deprogram* causes someone to become a Drunk, they become a Hacker instead.
**Tags:**
- Good
- Investigative/Support
- Human
- Unique
- Achievable""",colour=0x5dff00)
hacker.set_thumbnail(url=icons["hacker"])

hangman = discord.Embed(description="""*Behind the black veil, nobody truly knows what twisted thoughts are swimming through the dark void of his psyche. All we know is that the poor soul who incurred his wrath better start running.*
**Abilities:**
- Cannot be given to Neutral players.
- Is given the name of a randomly determined player of the same alignment to be their Prey at the start of the game.
- Any Lynching against their Prey is an Unstoppable Attack rather than a Standard Attack.
- If this player's alignment changes from Good to Evil or vice versa, they lose this modifier and gain the Romantic modifier, with their Prey becoming their Crush.
**Objectives:**
- Live to see their Prey be lynched by the town. The Hangman still wins if their Prey is resurrected by any means.
**Tags:**
- Modifier""",colour=0xbcbcbc)
hangman.set_thumbnail(url=icons["hangman"])

heir = discord.Embed(description="""**Actions:**
*Heritage* - During NIGHT 1, must choose a player to be their Loved One. This action may be used whenever the Heir does not have a Loved One. The Heir is told their Loved One's role.
*Contact* - Every night, may send a message to their Loved One. Their Loved One is told that this is a Whisper.
*Disown* - Once per game, may cause their current Loved One to lose that status.
*Smother* - Starts the game with no uses of this action. At night, may choose a player to target with a Standard Attack.
**Abilities:**
- If their Loved One dies, the Heir gains a Queued Save for each Attack their Loved One has dealt with any of their actions or abilities since becoming their Loved One, of equal strength to those attacks and in the same order, and on the next night gains an amount of uses of *Smother* equal to the amount of Saves that have been given by any of the Loved One's actions or abilities since they became a Loved One.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Killing
- Human
- Unique""",colour=0xff2323)
heir.set_thumbnail(url=icons["heir"])

hooligan = discord.Embed(description="""*There are good men. There are heroes, there are angels, there are saints and there are paragons. But there are bad men. Men who'll crack your skull open with knuckledusters if you look at them the wrong way. Men who'll make blood pour from your mouth like the first plague of Egypt. Men who'll gladly take your money and your woman without a second thought, safe in the knowledge that you'll do absolutely fucking nothing about it. And make no mistake, I am a very, very bad man.*
**Actions:**
*Threaten* - Every night, may choose one player. For each Attack that targets that player, the Hooligan targets them with another of equal strength. The Hooligan is told how many attacks they targeted the player with, if any.
*Bully* - Every other night, may choose a player. This player cannot be the same player targeted by *Threaten*. All of this player’s Active and Lunar saves are removed.
*Etch* - Once per game, may have two real words or players’ names sent to the Wolves chat. The Wolves are told that this is from a Hooligan. The Hooligan is told if no Wolves are alive.
**Abilities:**
- Is told the identity of the Direwolf at the start of the game.
- If they are Lynched, even if Saved, on the following night *Maul* is a Powerful Attack rather than a Strong Attack.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Killing/Support
- Human""",colour=0xff2323)
hooligan.set_thumbnail(url=icons["hooligan"])

hunter = discord.Embed(description="""*With a hare roasting over an open flame and a bullet in the chamber, his eyes scan every trunk, every leaf, every passing grain of dirt. Another wolf infestation? These mutts should know that these are* his *woods. And if they want to take it from him? They'll have to rip his throat out first.*
**Actions:**
*Shoot* - If attacked or lynched, is given the opportunity to target any player with a Standard Attack. This Attack is instead Powerful if the target is a non-Human. This still takes effect if they are Saved.
*Martyr* - Once per game, at Night, can choose to make all actions that Attack target them instead of the chosen target. Attacks from *Shoot* are unnaffected by this action.
**Abilities:**
- Starts the game with a Queued Strong Save.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Counteractive/Killing
- Human""",colour=0x5dff00)
hunter.set_thumbnail(url=icons["hunter"])

inventor = discord.Embed(description="""*Being top of the class is not enough. Having twenty-seven Ph.Ds under your belt is not enough. Having an IQ exceeding double the human possible total is not enough. Being a revolutionary, once-in-a-lifetime genius, academic and philosopher is not enough. Being the most intelligent being in the universe is not enough. To hold true power, you must first be able to demonstrate it.*
**Actions:**
*Doomsday Device* - Every night, may choose to activate their doomsday device. This targets all players targeting the Inventor with any action and the Inventor themselves with an Unstoppable Attack. If this Attack results in the deaths of two or more other players, the Inventor is Saved from all Attacks that night including the Attack from the doomsday device. On the night that *Doomsday Device* is used, the Inventor may not change role or alignment, though may gain or lose modifiers.
**Abilities:**
- If targeted with *Invite*, becomes a Tardis Engineer.
- If targeted with *Infect*, becomes a Cyberhound.
- If targeted with *Investigate*, becomes a Hacker.
- If any of the above abilities are activated in the same night as one another, their affects are nullified and the Inventor uses *Doomsday Device*.
- If targeted by *Haunt*, removes the player who targeted them's ability to use *Haunt* and does not commit Suicide.
**Objectives:**
- Be the last player alive, or finish the game with no living players.
**Tags:**
- Neutral
- Chaos/Killing
- Human
- Unique""",colour=0xff9400)
inventor.set_thumbnail(url=icons["inventor"])

jailor = discord.Embed(description="""**Actions:**
*Jail* - May jail one player every night. None of that player's actions that night take effect, and the player does not count as targeting anybody, and no actions that target that player take effect (if an action has multiple targets including the jailed player, it still affects the other players providing it does not need to affect the jailed player to do so) and the jailed player is told that thye have been jailed. If the jailed player is targeted with a Powerful or stronger Attack, the Jailor is targeted instead. The jailed player still commits Suicide if an action would cause them to.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Counteractive/Protective
- Human
- Unique""",colour=0x5dff00)
jailor.set_thumbnail(url=icons["jailor"])

# jester = discord.Embed(description="""*Look at the King, ladies and gentlemen! Look how silly he is with his crown and sceptre! Wouldn't it be funny if I threw this pie at his face? Wouldn't that be embarrassing? Wouldn't it be kooky if I tied his laces together or put a whoopee cushion under his chair? Wouldn't that be wacky? Hey, wouldn't it be fun if I slit his whore of a wife's fucking throat? Wouldn't it be nutty if I sacrificed all of his subjects to the voices in my own deranged fucking head? How about if I tortured him psychologically? If I made him my bitch?! If I fucked him in the head until the only way out was the blade of a knife? Wouldn't that be fun?!*
# **Actions:**
# *Haunt* - After dying from being Lynched, every night, the Jester chooses one player who voted against them in the Lynching to haunt. This player commits suicide on that night.
# **Abilities:**
# - If targeted with *Investigate*, then their identity is revealed to every Witch in the game.
# - If killed by a Lynching, gains the Spectre modifier.
# **Objectives:**
# - Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
# **Tags:**
# - Evil
# - Chaos/Killing
# - Ethereal""",colour=0xff2323)
# jester.set_thumbnail(url=icons["jester"])

knight = discord.Embed(description="""*From the day I was knighted, I've followed my vows. To stand up for the common folk and to be their shield against danger. I promised that I would seek out evil and destroy it in whatever form it may be. Even now, in these dark and troubled times, I will not falter or back down. I will be a beacon of hope for the hopeless and the downtrodden, and for all creatures of the night - beware my blade.*
**Actions:**
*Strike* - Every night, can choose one player to target with a Strong Attack. If they are Saved, a coin is flipped, and on the result of a heads that player's alignment changes to the same as the Knight's if it was not already. The Knight is not told the result of the coin flip.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Killing/Support
- Human
- Unique""",colour=0x5dff00)
knight.set_thumbnail(url=icons["knight"])

kresnik = discord.Embed(description="""**Actions:**
*Pulse Check* - Once per game, at night, may choose to receive a list of all Evil non-Human roles currently present in the game. The Kresnik is not told the number of each role present.
*Transform* - Once every two days, may choose to transform. At the beginning of the next night, the Kresnik's role changes to a random Good role. At the beginning of the following day, the Kresnik changes back to a Kresnik, providing that their role has not been changed by means other than this action since this action was used.
*Silver Blade* - Every night, may target any player with a Standard Attack. If the target is a Vampire, Cyberhound, Bloodhound, Cultist or Warlock, this Attack is Powerful rather than Standard.
**Abilities:**
- If the Kresnik would become a Vampire due to any action, both the Kresnik and the player using the action (randomly chosen in the case of multiple) commit Suicide.
- The Kresnik is twice as likely to appear in a game where any player is a Vampire.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Investigative/Killing
- Arcane
- Unique""",colour=0x5dff00)
kresnik.set_thumbnail(url=icons["kresnik"])

lich = discord.Embed(description="""**Actions:**
*Horcrux* - Twice per game at night while alive, can place a marker on any player. They gain an Active Standard Save, and become aware that they are marked by a Lich. If this action is used twice, the first target loses their marker.
*Undying* - Once per game at night while dead and the marked player is alive, may resurrect themselves.
**Abilities:**
- If resurrected by any means, becomes marked player's role however with the Lich's original alignment. The marked player is targeted a Powerful Attack. If they die from this, gain a Queued Standard Save and the death message states the marked player has committed Suicide. If resurrected by multiple actions within one night, the marked player is targeted with an equivalent number of attacks.
- Cannot be resurrected once the marked player is dead by any means. If any action causes this role to be resurrected, it fails.
- If *Horcrux* is used by any role other than the Lich, any previous targets of Horcrux lose their marker.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
-Evil
-Killing
-Ethereal
-Unique""",colour=0xff2323)
lich.set_thumbnail(url=icons["lich"])

lonewolf = discord.Embed(description="""**Actions:**
*Maul* - Discuss with other Wolves in a private channel who to Strong Attack during the night.
*Pack Offensive* - Once per game, at night, not before NIGHT 3, instead of using *Maul*, the Wolves may choose to each individually target any player with a Standard Attack.
*Terrify* - If they are the only Wolf left alive, every night, as well as using Maul, may target one player and cause all of their actions' targets to be replaced with themself.
**Abilities**:
- May still speak in the Wolf channel when dead.
- If there is only one other Wolf alive and the Direwolf is not alive, the Lone Wolf has final say on the target of *Maul*. Otherwise, the other Wolves have final say.
- Is immune to the effects of *Maul*.
- If they are the Mayor or Deputy, appears as a random Good Investigative role when targeted with *Investigate*.
- The other Wolves are not told that this player is a Lone Wolf.
- If at any point all players left alive are Wolves, it is announced that a Lone Wolf is present.
- Cannot change alignment or objective by any means, unless they also change role. If an effect would cause the Lone Wolf to change their alignment or objective without also changing their role, it fails.
- May only appear as a replacement for a Werewolf in role list generation where there would ordinarily be two or more Werewolves.
**Objectives:**
- Be the last player alive.
**Tags:**
- Neutral
- Counteractive/Killing
- Wolf
- Unique""",colour=0xff9400)
lonewolf.set_thumbnail(url=icons["lonewolf"])

mage = discord.Embed(description="""*Upon the peak of the village's mountain, there is always an old man practising the cryptic arts. Those daring enough to climb the mountain will always be greeted with a soothing cup of tea and a few surprising tricks.  Rumour has it that every few thousand years, when the cosmic order is in balance, he may descend back to the people he has watched over for eras. Unfortunately, the world he has returned to is now one of war and anarchy. It appears he may have to utilise his magic once more to rid the village of this evil...*
**Actions:**
*Switch* - Every night, may choose two players. The Mage learns if those players have the same alignment or not. For that night, all actions other than *Switch* that target the first player instead target the second player and vice-versa. If two Mages choose any of the same players to target with *Switch*, both of these actions fail. All rules referring to *Investigate* also act on *Switch*, other than those in the Seer's role description.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Chaos/Investigative
- Arcane""",colour=0x5dff00)
mage.set_thumbnail(url=icons["mage"])

medium = discord.Embed(description="""*The corpses pile up as a lone tear rolls down her cheek. They try dragging her away from her precious graveyard. "Your friends are buried," they say, "it's time to move on." It's not true - she's sure of it. She may be alone, but she can still hear their voices.*
**Actions:**
*Seance* - Once per game, once dead, may choose a living player to speak to for the night.
*Revitalise* - Once per game, at night, may choose any dead player to resurrect.
**Abilities:**
- Gains a Lunar Standard Save at the start of every night.
- Can speak in the #dead channel while alive and is permitted to repeat anything the dead have said.
- If targeted with *Investigate*, then their identity is revealed to every Witch in the game.
- If targeted by *Haunt*, removes the player who targeted them's ability to use *Haunt* and does not commit Suicide.
- Gains the Spectre modifier on death.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Support
- Arcane""",colour=0x5dff00)
medium.set_thumbnail(url=icons["medium"])

minstrel = discord.Embed(description="""*Fingers eloquently dancing to the command of his master, the Minstrel plays a melody of despair. Whether it's the arsonist filling his canister of oil or the medium crying out to her imaginary friends, one thing's for certain - somebody's going to die tonight.*
**Abilities:**
- See Bard (‘*w.roles_bard*’).
**Objectives:**
- Survive alongside the Bard at the end of the game.
**Tags:**
- Faction: *Troupe*
- Modifier
- Achievable""",colour=0xff0098)
minstrel.set_thumbnail(url=icons["minstrel"])

morty = discord.Embed(description="""*O-o-oh, jeez, Rick, y-y-you threw me into a portal and now I'm... I'm in a town with wolves and... and... oh, Rick, I'm looking around this place and I'm starting to w-work up some anxiety about this whole thing.*
**Actions:**
*Brainwave* - If the player this Modifier is attached to is not a member of a faction, once per game, at night, they may choose any role and alignment to appear as if targeted with *Investigate*.
**Abilities:**
- If the player this Modifier is attached to is a member of a faction other than Good and Evil, the other members of their faction appear as a random Good Human role (generated seperately each time) when targeted with *Investigate* rather than their actual role. The Morty, however, appears as their actual role when investigated.
- If this player votes in an election or a lynching during the day, their actions may not be used during the following night.
**Tags:**
- Modifier""",colour=0xfffa00)
morty.set_thumbnail(url=icons["morty"])

noir = discord.Embed(description="""*Just another case. It's not the same when it's your seventeenth year on the job, countless partners dead and countless criminals caught. Alls I know is I'm gonna do what I always do. I'm gonna catch every one of these lowlife fucks and make them pay. After all, it's just another case.*
**Actions:**
*Interrogate* - At night, chooses a player and guesses any category (Chaos, Counteractive, Investigative, Killing, Protective or Support). They learn that player's categories and if they guessed correctly that player is targeted with a Standard Attack. If that player survives, the following day any Lynch against them is a Powerful Attack rather than a Standard Attack. All rules referring to *Investigate* also act on *Interrogate*, other than those in the Seer's role description.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Investigative/Killing
- Human
- Unique""",colour=0x5dff00)
noir.set_thumbnail(url=icons["noir"])

paladin = discord.Embed(description="""*There once was a man who rode on horseback into our town, warning of a darkness that could never be quenched. He wore a glimmering ivory suit of armour and spoke with a voice that has seen an infinite number of years and lived to tell of them dozens of times over. His helmet's visor is able to be seen through but no man has ever had the hubris to believe themselves worthy of such an experience. It is said that those who gaze directly at his unmasked countenance will see the very eyes of the Lord staring back at them.*
**Actions:**
*Enlist* - Twice per game, may choose one player to force to guard another player of their choice. The latter player may not be the Paladin. The former player may not make any actions that night and any Attacks targeting the second player instead target the former. If any effect would cause the second player to commit Suicide, the former player instead commits Suicide.
*Bless* - Once per game, may target a different player. All of this player's actions that rules for *Investigate* apply to may be used twice that night, targeting different players.
**Abilities:**
- If an Attack is targeted towards an Evil role due to *Enlist*, the Paladin gains another use of *Enlist*. Only one use of *Enlist* may be gained by this method per night.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Counteractive/Protective
- Arcane
- Unique
- Achievable""",colour=0x5dff00)
paladin.set_thumbnail(url=icons["paladin"])

pixie = discord.Embed(description="""*Often found pestering those sleeping in the day. This talkative spirit flitters between the people, jumping from one choice to the next and deciding things out of pure luck. Maybe that’s how they ended up here, in this luckless town.*
**Actions:**
*Flip* - Told whenever they are being targeted by any action, including a Lynching. They are given the title of the action and asked if they want to try and flip one of their pennies to be unaffected by this action. On a heads, they are unaffected. May only flip once per action. If an action would not take effect by another means, the Pixie is not informed of it.
*Identify* - At night, may flip a penny and target any player to be told that player's alignment on a heads. All rules referring to *Investigate* also act on *Identify*, other than those in the Seer's role description. If the target is lynched on the following day, the Pixie gains two pennies
*Ascend* - Once per game, after NIGHT 3, if they have more pennies than they started with, can become an Archfey. This action cannot fail by any means.
**Abilities:**
- At the start of the game, has an amount of pennies equal to half the total amount of players rounded down.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Counteractive/Investigative
- Unearthly""",colour=0x5dff00)
pixie.set_thumbnail(url=icons["pixie"])

politician = discord.Embed(description="""*“Our great democracies still tend to think that a stupid man is more likely to be honest than a clever man, and our politicians take advantage of this prejudice by pretending to be even more stupid than nature made them.” - Bertrand Russell*
**Actions:**
*Rig* - If the Politician is Mayor or Deputy, once per game they can change the result of the Lynching. The Game Master announces the change and the new lynchee but not the Politician's identity.
*Regicide* - If the Politician is not Mayor or Deputy, once per game, at night, they can hang the Mayor, targeting them with an Unstoppable Attack. For all rules purposes, this Attack counts as a Lynching. The day after this action is used, the Attack from a Lynching is a Powerful Attack rather than a Standard Attack.
*Gaslight* - Every night, may choose any role and alignment to appear as if targeted with *Investigate*.
**Abilities:**
- Gains a Lunar Powerful Save at the start of every night they do not use *Gaslight*.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Killing/Counteractive
- Human
- Unique""",colour=0xff2323)
politician.set_thumbnail(url=icons["politician"])

poltergeist = discord.Embed(description="""*You can find many creatures of the night, but they are all nothing compared to it. With its long, slender fingers controlling every piece on the board, it is the unparalleled monarch of twilight as it swarms the chambers of every man and beast to cross it, lacing the tight noose around our throats in the gallows of our nightmares; our deaths a morsel for it to devour and our souls a trophy for its triumph. You aren't afraid of the dark, are you?*
**Actions:**
*Redirect* - Every night, can choose two players; one to redirect and one for the first player to redirect towards. All of the former player's actions that have targets used that night have their first target changed to the second player. The first player is not aware of this. The Poltergeist learns the first player’s role and alignment, in the same fashion as a Seer's *Investigate*. All rules referring to *Investigate* also act on *Redirect*, other than those in the Seer's role description. If two Poltergeists choose the same player as the first player, that player's actions fail and hence target nobody.
**Abilties**
- If two Poltergeists attempt to redirect each other, their actions fail and they both die and gain the Spectre modifier.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Chaos/Investigative
- Ethereal""",colour=0xff2323)
poltergeist.set_thumbnail(url=icons["poltergeist"])

poser = discord.Embed(description="""*Aw, you kids these days with your Snapchat and your Pokeymans and your John Green books and your insatiable lust to be "aesthetic". Y'see, I'm not like most adults. I'm cool. I'm only 38 and I signed up for a Tumblr account last night. I'm no old-timer. I love playing these Zelda games too - he's the coolest little elf boy. I get you. I'm cool, right? C'mon, tell me how cool I am.*
**Actions:**
*Pose* - Every night, chooses another player. That player's top-listed action is selected and the Poser uses it as if it was their own action. Any action that is limited-use is freely usable for the purpose of this action. If the action requires a choice to be made by the player using it, such as a target, the Poser is given the minimum information needed when presented with this choice.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Chaos
- Human""",colour=0x5dff00)
poser.set_thumbnail(url=icons["poser"])

priest = discord.Embed(description="""*As your teacher, I bid you, say it with me- Children of the Lord! The great devotion shall not be swayed, we shall be exalted! We shall see the rise of our one true saviour and live to see the day this world is cured of its sickness!*
**Actions:**
*Convert* - Once per game, can choose one player to convert to Good. In the event that this player is already Good, the Priest may not try again.
*Sacrifice* - Once per game, at night, can choose two players, one of whom is already dead. The dead player is revived as a random non-Unique Good role, and the living player commits Suicide.
*Purify* - Once per game, can remove all Lunar and Active Saves for Evil roles.
**Abilities:**
- The presence of the Priest is revealed at the start of the game, but not their identity.
- If the Priest is present, the Cultist must also be present.
- At the start of NIGHT 4, becomes a Paladin.
- If alignment is changed to Evil, becomes a Cultist.
- If any Cultist becomes a Priest, becomes a Cultist.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Counteractive/Support
- Human
- Unique""",colour=0x5dff00)
priest.set_thumbnail(url=icons["priest"])

prince = discord.Embed(description="""**Actions:**
*Praise* - Every night, may target any player other than themselves, who they have not already used this action on. Any limited-use actions that player may have are put in the state they were in at the beginning of the game.
**Abilities:**
- If killed by any means other than Suicide, the day after their death is skipped, meaning that there are two nights taken consecutively.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Support
- Human
- Unique""",colour=0x5dff00)
prince.set_thumbnail(url=icons["prince"])

psychic = discord.Embed(description="""**Actions:**
*Predict* - Every night, may choose any player and guess the name of any action. If the target player has an action of that name, the Psychic gains one use of that action. If the action is limited use, the target player loses a use of the action. If the target player does not have an action of that name, they are told that a Psychic attempted to predict their actions. If a Psychic successfully predicts *Infect*, they become a Cyberhound.
*Vessel* - Every night, or upon receiving the results of *Predict*, the Psychic may choose any player and choose any action that they have gained a use of through *Predict*. The chosen player gains a use of this action and the Psychic loses it.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Chaos/Support
- Arcane
- Unique""",colour=0xff2323)
psychic.set_thumbnail(url=icons["psychic"])

researcher = discord.Embed(description="""*The laptop screen reflected off his lenses as he scanned each and every pixel of the monitor's display. It was now 3:15am but he was not deterred, not even slightly. His right hand's index finger scrolled intensely through the files on his computer. He was going to find it and he was not going to be distracted by whether that pungent aroma was his own sweat or the half-eaten ramen on his desk. Even if it meant he would have to rip apart this entire goddamn town, he was going to find these wolves.*
**Actions:**
*Research* - At night, can choose a player to observe. The Researcher is told all players that targeted the target player (in the event that the Wolves target them, the Researcher is only told that the Direwolf did, however if the Direwolf is dead then all Wolves are shown to target them). On the night that the Researcher used this action, no saves they have are taken into effect.
*Stakeout* - Once per game, after NIGHT 3, can choose one player. For the rest of the game, at the start of every day, learns that player’s targets from the previous night.
**Abilities:**
- Gains a Lunar Standard Save at the start of every night.
- If they target a player with *Research* and at least half of the living players (rounded up) appear in the results, the Researcher becomes a Hacker.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Investigative
- Human
- Unique""",colour=0x5dff00)
researcher.set_thumbnail(url=icons["researcher"])

rogue = discord.Embed(description="""*The fire in her eyes could warm the cool winter breeze. Finally, she had returned back to her own cut of paradise - where the pockets ran deep and the toys came out to play. Except this time, there was something different. An invading force had the people subdued and fearful. Paranoia caused everyone to lock their doors and hide their wallets. Somebody had taken her village. Somebody was going to pay.*
**Actions:**
*Steal* - Every night, may choose two players. All of the first player's Saves are removed and given to the second player. For each Save the first player had, the Rogue gains an identical Save.
*Donate* - Every night, may be told all the saves they have, and choose one Save to give to a player of their choice. This Save is removed from the Rogue.
**Abilities:**
- All Attacks targeting the Rogue are increased in strength by two levels to a maximum of Unstoppable.
- If they become Evil, they become a Dodomeki.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Counteractive/Protective
- Human
- Unique""",colour=0x5dff00)
rogue.set_thumbnail(url=icons["rogue"])

romantic = discord.Embed(description="""*You try to treat your beloved well, of course, but all the clichés in the world won't be able to save them from the mayor's vote, a wolf's claws, or the blade of a knight. It's time to take action now, or else your love might die forever, your burning passion sent up in flames.*
**Abilities:**
- Cannot be given to Neutral players.
- Is given the name of a randomly determined player of the opposite alignment to be their Crush at the start of the game.
- If their Crush is killed, they commit Suicide on the next night.
- If this player's alignment changes from Good to Evil or vice versa, they lose this modifier and gain the Hangman modifier, with their Crush becoming their Prey.
**Objectives:**
- Have their Crush be alive at the end of the game.
**Tags:**
- Modifier""",colour=0xbcbcbc)
romantic.set_thumbnail(url=icons["romantic"])

santa = discord.Embed(description="""**Actions:**
*Gift* - Every night, can give any player other than themselves a present. They receive the present the next night. If during the day between these nights, the targeted player votes against a player of their own alignment in the Lynching, they are told that they have received a lump of coal. For that night, they gain the ability to target any player with a Powerful Attack. Santa is immune to this Attack. Otherwise, the player is told that they have received a candy cane, and that player and Santa both receive a Queued Standard Save.
*Premium* - Every other night, may use *Gift* twice rather than once, however may not use *Gift* on the following night.
**Abilities:**
- If any effect would cause them to commit Suicide, they can choose to instead be targeted with a Powerful Attack.
**Objectives:**
- Have all other living players have received a present at the point of your death, or all other living players have received a present at the end of the game if you are still alive.
**Tags:**
- Neutral
- Support
- Unearthly
- Unique""",colour=0xff9bc4)
santa.set_thumbnail(url=icons["santa"])

scarecrow = discord.Embed(description="""*There's something dark in this town. I can feel it in my bones, and it's everywhere. Even in my fields, something's different. Every time I wake at morning, something's changed, something's not how it was the night before. It gives me the creeps. At least I've my scarecrows watching over me.*
**Actions:**
*Curse* - Every night, selects one player. If that player dies during that night or the following day, they gain the Spectre modifier. Cannot choose the same player twice in a row.
**Abilities:**
- Gains an Active Unstoppable Save at the start of each day.
- Does not need to be eliminated to fulfil any Good role's objectives.
**Objectives:**
- Become a Spectre.
- Survive until the end of the game.
**Tags:**
- Neutral
- Chaos/Support
- Ethereal
- Unique""",colour=0xb7ffe8)
scarecrow.set_thumbnail(url=icons["scarecrow"])

scavenger = discord.Embed(description="""**Actions:**
*Scavenge* - Every night, choose a category (Chaos, Counteractive, Investigative, Killing, Protective or Support). A random non-Achievable role of that category is chosen, and the Scavenger is told this role. The Scavenger gains access to a single action of their choice from that role at the start of the following day and loses access to this action at the end of the following night. At the start of the following day, the role that was generated is announced.
*Reclaim* - Once per game, at any time, choose an action currently usable due to *Scavenge*. The Scavenger has access to this action for the rest of the game, and does not lose access at the end of the following night. If this action is not an every day, every night or reactionary use, the Scavenger gains a single use.
**Abilities:**
- The presence of the Scavenger is announced at the start of the game.
- Is told the intended target of any action whenever it is redirected, and the target that the action is redirected to.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Chaos
- Human""",colour=0x5dff00)
scavenger.set_thumbnail(url=icons["scavenger"])


seer = discord.Embed(description="""*Her silence, well, that's the worst part of it. She doesn't raise her voice, she doesn't cry out, it's just her deathly silence. Her silver pupils scan the room and they'll lock onto you. And she then she just points. There are no words, no anger, no sorrow - only the stare. And as you're being dragged away to the gallows, her apathy is unwavering. No smirks, no grins, just the cold and unfeeling gaze. And then, as the rope is being tightened around your neck, her silence is the last thing you'll ever hear.*
**Actions:**
*Investigate* - Every night, may choose one player to investigate, and is told that player’s role and alignment, but not any Modifiers.
*Publish* - Once per game, at night, or upon recieving the results of *Investigate*, the Seer can choose to anonymously announce the results of a previous investigation publicly to all players at the start of the next day.
**Abilities:**
- Whenever they get a result of Evil on a player whom they have not previously targeted with *Investigate*, they gain a Queued Strong Save.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Investigative
- Arcane""",colour=0x5dff00)
seer.set_thumbnail(url=icons["seer"])

sentinel = discord.Embed(description="""**Actions:**
*Shield* - Every night, may choose a Player. That night, all Attacks targeting the Player instead target the Sentinel.
*Parry* - Once per game, at night, when not using Thrust, can choose to give themselves a Lunar Powerful Save. If the Save takes effect, they gain a Queued Powerful Save.
*Thrust* - Once per game, at night, when not using Parry, can choose to give themselves a Lunar Powerful Save. If the Save takes effect, they target the player targeting them with a Strong Attack.
**Abilities:**
- Starts the game with three Queued Powerful Saves.
- Is told whenever they gain or lose a save, and what type it is.
- If an effect would cause them to lose all their saves, they instead lose only one.
**Objectives:**
- Successfully use up three Saves through using Shield.
**Tags:**
- Neutral
- Protective
- Unearthly""",colour=0x00f6ff)
sentinel.set_thumbnail(url=icons["sentinel"])

sharpshooter = discord.Embed(description="""**Actions:**
*Scope* - Every night, may choose one player to scope, and is told that player’s role and alignment, but not any Modifiers. The Save that player gained most recently is removed. All rules referring to *Investigate* also act on *Scope*, other than those in the Seer's role description.
*Snipe* - Once per night, can choose any player and guess their role. If they guess the player's role correctly, that player is targeted with a Strong Attack. If the chosen player survives this attack, they lose their weakest save. Cannot be used in the same night as Scope. Cannot target the same player twice in a row. This action's target cannot be changed from the Sharpshooter's original choice by any other effect.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Investigative/Killing
- Human
- Unique""",colour=0x5dff00)
sharpshooter.set_thumbnail(url=icons["sharpshooter"])

shifter = discord.Embed(description="""*A dark presence darts around the townsfolk. Its flickering form gradually corrupts and dissolves the body's soul as it passes through - who knows what chaotic influence brought it into existance, or what force could ever take it out.*
**Actions:**
*Shift* - Once every two nights, select another player. The Shifter receives that player’s role, alignment, objectives and any tags or modifiers, while that player loses their role, alignment, objectives and tags or modifiers to become a Shifter. Queued Saves are not transferred in either of these role changes. If the target of this action has an Achievable role, the action fails. Achievable modifiers do not make any difference.
**Abilities:**
- If a Seer targets a Shifter with *Investigate*, the Seer immediately commits Suicide.
- If a Shifter targets another Shifter with *Shift*, they both immediately commit Suicide. Any player the other Shifter has targeted does not change role.
- If a player spends three nights in total as a Shifter (these do not need to be consecutive), they become a Souleater.
- If a player becomes a Shifter, any waiting period for *Shift* resets.
**Objectives:**
- None
**Tags:**
- Neutral
- Chaos
- Ethereal""", colour=0xd5ff77)
shifter.set_thumbnail(url=icons["shifter"])

slasher = discord.Embed(description="""**Actions:**
*Slaughter* - Every night, may choose one player to target with  a strong attack. On the night that the Slasher uses this action, any player who targets the Slasher with an attack will be targeted with a standard attack three nights later.
*Mask* - Once per game, at night, when not using *Slaughter*, may choose to use their mask. On the following day, if the Slasher is voted to be lynched, the lynch is changed to the second most voted player (randomly chosen in the event of a tie). It is announced that a Politician has used *Rig*.
*Legacy* - Once per game, only during NIGHT 1, the Slasher chooses a player. In the event of the Slasher's death at any point after this action is used, the chosen player becomes a Slasher.
**Abilities:**
- At the start of NIGHT 1, the Slasher gains two uses of *Mask*.
**Objectives:**
- Be the last player alive, or finish the game with no living players.
- Have another Slasher achieved through this Slasher's Legacy complete their objective.
**Tags:**
- Neutral
- Chaos/Killing
- Unearthly
- Unique""",colour=0xff9400)
slasher.set_thumbnail(url=icons["slasher"])

souleater = discord.Embed(description="""*Wake from your sleep. Free yourself of your skin. Rid yourself of these mortal shackles. Open your eyes. Come to me, my brother, and we shall walk to the promised land together. Today, we escape.*
**Actions:**
*Absorb* - Every two nights, can choose one player to gain the Soulless modifier.
*Evocation* - At any time, may choose any Soulless player and write a message which is given to them anonymously.
*Consume* - At any time, once per day or night, can choose one Soulless player to force to commit Suicide. This player can not have gained the Soulless modifier in the previous or same night.
*Obituary* - At any time, may be told the list of players with the Soulless modifier.
**Abilities:**
- Gains a Lunar Standard Save at the start of every night.
- If targeted with *Investigate*, the player who targeted them gains the Soulless modifier.
- If Souleater gains the Soulless modifier by any means, all players including the Souleater lose the modifier.
- Becomes a Spectre if killed by a Soulless player.
**Objectives:**
- Be the last player alive, or finish the game with no living players.
**Tags:**
- Neutral
- Chaos/Killing
- Ethereal
- Achievable""",colour=0xd5ff77)
souleater.set_thumbnail(url=icons["souleater"])

soulless = discord.Embed(description="""**Abilities:**
- See Souleater ('*w.roles_souleater*').
**Tags:**
- Modifier
- Achievable""",colour=0xd5ff77)
soulless.set_thumbnail(url=icons["soulless"])

spectre = discord.Embed(description="""*Whether your heart is full of gold or ice, whether your lungs are full of nitrogen or ectoplasm, whether your hand holds a stick or a sceptre, there are four words that shall forever echo in the hearts of men, gods and beasts. Four words that cross the divine border and unite kings and pawns. A mantra that can resist shimmering fangs and slashing blades: **I Will Not Die**.*
**Abilities:**
- This modifier may only be applied to a dead player. If a player with the Spectre modifier is resurrected they lose the modifier and do not regain it if they die again. If a player gains the Spectre modifier by any means when alive, they die, but keep the modifier.
- Players with this modifier may use their actions and abilities as if they were living players. They may target living or dead players, however these actions only take effect on dead players if they also have the Spectre modifier.
- If Attacked by another Spectre and cannot Save against the Attack, the player loses the Spectre modifier and is not resurrected.
- Players with the Spectre modifier may only speak in the #dead chat like ordinary dead players, unless they are a Medium. Mediums with the Spectre modifier may vote in elections and lynchings.
- A Shifter with the Spectre modifier is resurrected if they switch roles with a living player, however their target does not retain the Spectre modifier and dies, unless they were dead and already had the modifier.
**Objectives:**
- Players with the Spectre modifier still pursue their objectives, however they do not count as living.
**Tags:**
- Modifier
- Achievable""",colour=0xb7ffe8)
spectre.set_thumbnail(url=icons["spectre"])

spider = discord.Embed(description="""*The web of pain and sin it weaves will often go unnoticed. Whilst we're busy fighting wolves, gods and demons, it makes its way into our consciences, threading its little fears in there. It'll lace its hands around the bodies of the dead, breathing wicked life into these cold lungs like the cruel seductress it truly is, the dark mother of all of us... I mean, them. Oh, shit, I wasn't meant to say that, was I? You won't tell anybody, will you? Nah, she'll make sure you don't.*
**Actions:**
*Thread* - Once per game, at night, can use their Thread and choose any dead player. The #dead chat is given the chance to vote collectively on which dead player is to be revived, and the player chosen by the Spider gains an extra vote, and wins all ties. The player revived as a result of this action gains a Queued Powerful Save.
*Web* - This action has three charges. At night, can choose to activate their Web and choose an amount of charges to use. This action may not be used once all charges are depleted. All players who target them that night have all their actions fail that night, and for an amount of subsequent nights equal to one less than the amount of charges used.
**Abilties**
- If killed by Wolves, becomes a Spectre.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Counteractive/Support
- Unearthly
- Unique""",colour=0xff2323)
spider.set_thumbnail(url=icons["spider"])

spy = discord.Embed(description="""**Actions:**
*Infiltrate* - Every night, can target any player. The spy is told any Modifiers that player has, and the names of all of the Private Channels that they are in.
*Surveillance* - Once per game, at night, may choose to receive the amount of Good, Neutral and Evil players currently alive. A random one of the numbers given will differ from the actual value by one.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Investigative
- Human""",colour=0x5dff00)
spy.set_thumbnail(url=icons["spy"])

standuser = discord.Embed(description="""*Standing over his opponent, the man looks down in contempt at him, a pitiful wreck asking for forgiveness. "Good grief." A shimmering muscular figure materialises, fists laden with gold, hair waving in the cold wind. "There's a reason you lost." In a swift motion, he traces a finger along the rim of his cap. "You pissed me off."*
**Actions:**
*Battle Cry* - Once per game, after using any action, may use that action immediately again without expending a use, even if they have no uses remaining. If an action used using *Battle Cry* would usually use up anything from a limited pool, it does not. If this action is used on an action used by a faction, if each member of the faction were to do something individually from the action only the Stand User gains an additional use of this.
*Stand Arrow* - Upon death, may choose to flip a coin. On a heads, a player of their choice gains the Stand User modifier.
*Clash* - Once per game, can choose to target any player with a Powerful Attack. Players are only affected by this attack if they have the Stand User modifier. If the chosen player dies as a result of this attack, this player gains another use of *Battle Cry*.
**Abilities:**
- If any Stand User exists in a game, at least one other player must also have the Stand User modifier.
- If uses *Battle Cry* on the same night as any other player uses *Battle Cry* (and neither action fails), commits Suicide.
- If targeted with *Clash* by a player who they use *Clash* on during the same night, commits Suicide.
- If targeted with *Stand Arrow*, gains a Queued Unstoppable Save, a use of *Clash* and a use of *Battle Cry*.
**Objectives:**
- Kill another Stand User through *Clash*, or have a player who became a Stand User due to this player's use of *Stand Arrow* complete this objective.
**Tags:**
- Modifier""",colour=0x80659a)
standuser.set_thumbnail(url=icons["standuser"])

survivalist = discord.Embed(description="""*A million miles from home, in some castaway village of savages violently fucking and killing anything that moves. Each breath is a battle against the odds, each step is another potential landmine, each confrontation is a test of willpower where only the bravest can survive. But me? I don't just survive. I thrive. I live. And, if you dare cross me, I'll make sure you can't say the same.*
**Actions:**
*Bulletproof Vest* - Three times per game, at night, may choose to gain an Active Unstoppable Save.
*Paranoia* - Once per game, at night, may choose to cause all players targeting the Survivalist to have their action fail and to be instead targeted with a Standard Attack.
**Abilities:**
- If any effect would cause them to commit Suicide, it fails.
- Is told the identity of any other Survivalists in the game.
- Does not need to be killed to fulfill any other role's objectives.
**Objectives:**
- Survive until the end of the game.
**Tags:**
- Neutral
- Counteractive
- Human""",colour=0x00ff85)
survivalist.set_thumbnail(url=icons["survivalist"])

# sylph = discord.Embed(description="""*Death is an inevitability - the end of all things. In time, all shall bow before it. There is no man, beast or god who can resist the seductive allure of the reaper's scythe and there is nothing to ever come back from it. We must all concede inevitably - we have no say in if we die, but we do have a say in how. Whether in a blaze of glory or a wave of the blood of our enemies, we must all accept that there's no magical guardian out there to save us from it. Such a fantasy would be... ridiculous.*
# **Actions:**
# *Revitalise* - Once per game, at night, may choose any dead player to resurrect.
# **Abilities:**
# - Gains the Spectre modifier on death.
# - Gains a Lunar Standard Save at the start of every night.
# **Objectives:**
# - Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
# **Tags:**
# - Good
# - Support
# - Ethereal
# - Unique""",colour=0x5dff00)
# sylph.set_thumbnail(url=icons["sylph"])

tardisengineer = discord.Embed(description="""*Sure, it works the ship like a charm, but that aberration never stops groaning in its robotic tone. I hate to think about what remains of the young man's consciousness. A jumble of wires, mechanics and Cyberman parts - I really wonder what kind of drugs I was on when I decided to make this thing. I also really wonder if the TARDIS saved their planet of origin so I can get some more.*
**Actions:**
*Activate TARDIS* - Once per game, during the day, can choose to activate the TARDIS. At that same point in the next day (based around elections and lynchings), the whole town is told that time has been undone and the game goes back to the point when the TARDIS was activated. Any uses of actions and kills after that point are undone, however all knowledge gained remains known. The game continues from the TARDIS activation point. If the TARDIS Engineer’s role changes before time is undone, this action still takes effect.
**Abilities:**
- If they lose the Companion modifier, they become an Inventor, however they keep the alignment and objective they had as a TARDIS Engineer.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Protective/Support
- Human
- Faction: *Tardis*
- Unique
- Achievable""",colour=0x204eff)
tardisengineer.set_thumbnail(url=icons["tardisengineer"])

thief = discord.Embed(description="""*She knows this place. She knows the drunk who couldn't protect his wallet if his life depended on it. She knows the old lady who leaves her window open so God can watch over her better. She knows the kid who forgets to lock the door whenever he goes to school. Easy pickings.*
**Actions:**
*Ambush* - Every night, sets a trap. Whoever is lynched the following day, even if Saved, has their first listed action stolen by the Thief. The Thief gains access to this action until *Ambush* is used again.
**Abilities:**
- If the Thief successfully receives *Investigate* or any similar actions through the use of *Ambush*, they become a Dodomeki.
- Upon death or changing role, all actions taken using *Ambush* are returned to the players they were stolen from, even if those players' roles have changed since the action was stolen.
- If the Thief uses a Killing action received through *Ambush*, the targeted player appears to have been Killed by the role where the action sourced from.
- If a Thief receives *Maul* or *Pack Offensive* from *Ambush*, all Wolves lose the action.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Chaos/Counteractive
- Human
- Unique""",colour=0xff2323)
thief.set_thumbnail(url=icons["thief"])

timelord = discord.Embed(description="""*Not much is known about him. A quirky stranger with a multicoloured scarf, a massive pair of ears and a bowtie just showed up one day and told me to trust him. I think he's lying... it would explain the quick heartbeat.*
**Actions:**
*Invite* - Once per game (regaining a use if the action fails), at night, may choose another player to gain the Companion modifer. This Companion joins the Time Lord’s channel. After the Time Lord regenerates, the Companion loses their Companion modifier. They also lose the modifier if they die or their alignment changes. If a player who already has the Companion modifier is targeted with *Invite*, or a player is targeted with *Invite* by two Time Lords of the same alignment on the same night, they become a TARDIS Engineer. If two Time Lords of different alignments target the same player with *Invite*, both actions fail, however the use of *Invite* is still used up.
*Sonic* - Once per game, may use their sonic device to retrieve a list of roles. The amount of roles given will be equal to the amount of players and at least half of the list, rounding up, will be present in the game. Which roles are chosen is randomly determined.
**Abilities:**
- Starts the game with a Queued Unstoppable Save called a regeneration. When this Save is used, the Time Lord’s alignment changes to Good or Evil, whichever of the two has less living players not including the Time Lord. If they are equal in size, the Time Lord changes to the opposite alignment. The Time Lord gains another use of *Invite*. In addition, the town is told that a Time Lord has regenerated, and their alignment after the regeneration.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated (If Good).
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated (If Evil).
**Tags:**
- Good
- Investigative/Support
- Unearthly
- Faction: *Tardis*""",colour=0x204eff)
timelord.set_thumbnail(url=icons["timelord"])

twin = discord.Embed(description="""*Two souls, eternally bonded. Our veins may pump different blood and our backgrounds may be different, but you will forever be my brother, in health and in sickness, in poverty and in richness, in peace and in war, in life and in death.*
**Abilities:**
- This Modifier is always applied to a pair of players.
- The Twins may speak in a shared channel with one another at any time.
- If one Twin dies by any means, the other commits Suicide the next night (this counts as taking an action).
- If one Twin is resurrected, the other is also resurrected.
- If one Twin gains the Spectre modifier, so does the other.
- This Modifier may not be applied to a Priest, Cultist or a Neutral Role.
- These Twins cannot change alignment by any means. Any action that changes their role keeps their alignment.
**Objectives:**
- Survive alongside your twin until the end of the same.
**Tags:**
- Modifier""",colour=0x9900ff)
twin.set_thumbnail(url=icons["twin"])

vampire = discord.Embed(description="""*Never sleep again. Never dream again. Make more room for nightmares, like the one you're having right now, where my claws are creating crimson crevaces in your dainty white skin, where my eyes pierce into whatever's left of your soul as your eyes beg the question of "why?", as my fangs explore and explode with delight at the taste of your almost-human blood. You blink and pinch yourself awake but there's one problem: it was never a nightmare in the first place.*
**Actions:**
*Fang* - Every two nights, all Vampires together may target any player. That player loses all of their saves. If that player had no saves, they become a Vampire. If the player targeted had also been targeted with Infect that night, was a Werewolf or had the Feral modifier, they become a Bloodhound rather than a Vampire.
*Stalk* - Once per game, at night, may choose to learn the identity of a random Wolf. 
**Abilities:**
- All Vampires and Bloodhounds in the game may speak in a collective private channel with one another. They may still speak in this channel after death.
- Whenever any player (other than one who was already a Vampire) becomes a Vampire or a Bloodhound, all Vampires and Bloodhounds other than that player gain a Queued Standard Save.
- If targeted by any effect that would cause an alignment change without changing the Vampire's role, commits Suicide.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Support
- Unearthly
- Faction: *Vampiric*
- Unique""",colour=0x9b0029)
vampire.set_thumbnail(url=icons["vampire"])

warlock = discord.Embed(description="""*The inky black fingers of the warlock are always wrapped firmly around his sceptre, his instrument of destruction. His muscular arms, both densely tattooed with sigils and spells written in an ancient script, had long, grey veins running up towards and around his neck, strangling him like a cobra. The village has never heard him speak, merely mutter incantations under his breath. There's an aura of fear about him, nobody speaks directly to him - there is no man foolhardy or brave enough. Some say he sold his soul to the devil eons ago. Few disagree.*
**Actions:**
*Damn* - Twice per game, at night, may choose a player to damn. The damned player and all players targeting them (if targeted by Wolves a random Wolf is chosen) apart from the Warlock are targeted with a Powerful Attack.
*Hell's Gate* - Once per game, may target one player. All actions that player makes on the night this action is used fails if they specifically target a Priest, Paladin, Cultist or Warlock.
**Abilities:**
- For each Good Investigative role they kill using *Damn*, the Warlock gains another use of *Damn*. Only one use of *Damn* may be gained by this method per night.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Chaos/Killing
- Arcane
- Unique
- Achievable""",colour=0xff2323)
warlock.set_thumbnail(url=icons["warlock"])

werewolf = discord.Embed(description="""*You think you can tell us what to do? You think you have any power over us? We are an army. We are a brotherhood. You humans can't stand a chance against us. We run this town now, bitch.*
**Actions:** 
*Maul* - Discuss with other Wolves in a private channel who to Strong Attack during the night.
*Pack Offensive* - Once per game, at night, not before NIGHT 3, instead of using *Maul*, the Wolves may choose to each individually target any player with a Standard Attack.
**Abilities:**
- May still speak in the Wolf channel when dead.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Killing
- Wolf
- Faction: *Wolves*""",colour=0xff5000)
werewolf.set_thumbnail(url=icons["werewolf"])

whisperer = discord.Embed(description="""*There is truly nothing as powerful and versatile as words. Three words can stand for love, six can topple a government, fourteen can show hate, a few thousand can give you a career. How many do you reckon it'd take to end a pack of wolves?*
**Actions:**
*Whisper* - Every night, chooses one role and write a message to be sent to that role. The Whisperer is told if the role is in the game or not, but if it is not in the game the Whisperer may not use this action again that night. The message is sent to all players with that role. Modifiers cannot be selected as targets for this action.
*Echo* - Once per game, while using *Whisper*, allows the target to send a message back to the Whisperer at the end of the same night.
**Abilities:**
- Gains the Spectre modifier on death if they die by any cause other than Suicide or Lynching.
**Objectives:**
- Have at least one Good role survive until all Evil and Neutral roles have been eliminated.
**Tags:**
- Good
- Investigative/Support
- Arcane
- Unique""",colour=0x5dff00)
whisperer.set_thumbnail(url=icons["whisperer"])

witch = discord.Embed(description="""*A being of neither light nor evil, yet more destructive than both. A seductress who'll turn brother against brother and kill their entire family. A vampire who lusts for blood yet bares no fangs. A sycophant, dedicated not to malice, but to chaos. She lies in wait, her primal instinct to worship her primordial deities and protect the sisterhood - something she'll do at all costs.*
**Actions:**
*Poison* - Target one player with a Standard Attack every night. If a player is targeted with *Poison* and *Heal* on the same night by two different players, they become a Witch.
*Heal* - May give one player an Active Standard Save every night. If a player is targeted with *Poison* and *Heal* on the same night by two different players, they become a Witch.
*Find Familiar* - Once per game, if this player was a Witch on DAY 1, can target a dead player. That player becomes a Witch and gains the Spectre modifier.
*Cackle* - Once per game, can select a player and a sentence. That player is told that a Witch has whispered that sentence.
**Abilities:**
- If the Medium is present and they are killed with *Poison*, every Witch becomes Evil and replaces their objective with ‘Have at least one Evil role survive until all Good and Neutral roles have been eliminated.’
- If the Jester is present and they are killed with *Poison*, every Witch becomes Good and replaces their objective with ‘Have at least one Good role survive until all Evil and Neutral roles have been eliminated.’
- Is told the number of Witches at the start of the game.
- Is told whenever a player becomes a Witch however is not told the identity of the player.
**Objectives:**
- Have at least one member of the Witches survive until all other roles have been eliminated.
**Tags:**
- Neutral
- Killing/Protective
- Unearthly
- Faction: *Witches*""",colour=0xd800ff)
witch.set_thumbnail(url=icons["witch"])
