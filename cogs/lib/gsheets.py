import gspread_asyncio

from oauth2client.service_account import ServiceAccountCredentials as SAC




def get_creds():
    return SAC.from_json_keyfile_name(
        'google_api.json',
        [
            'https://spreadsheets.google.com/feeds',
            'https://www.googleapis.com/auth/drive',
            'https://www.googleapis.com/auth/spreadsheets'
        ]
    )


class Gspread():

    def __init__(self):
        self.agcm = gspread_asyncio.AsyncioGspreadClientManager(get_creds)

    async def __aenter__(self):
        self.agc = await self.agcm.authorize()
        self.ss = await self.agc.open_by_url((
            "https://docs.google.com/spreadsheets/d/"
            "13KOfTZqdu305p4nhkjU2l14BnUAO6zTT6M0PCCiWohE/edit#gid=2038101782"
        ))
        self.ws = await self.ss.worksheet("Summary")

    async def read(self):
        return await self.ws.get_all_records()

    async def get_user(self, ID):
        records = await self.read()
        info = [
            d for d in records if d["USERID"] == ID or d["ALT USERID"] == ID
        ]
        if info:
            return info[0]
        return None

    async def get_list(self):
        records = await self.read()
        info = {d["USERID"]:d["NAME"] for d in records}
        return info

    async def __aexit__(self, exc_type, exc, tb):
        del self.agc, self.ss, self.ws

gsheets = Gspread()