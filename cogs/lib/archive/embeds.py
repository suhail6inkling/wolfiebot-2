import discord
from cogs.lib.archive import ArchiveIcons as icons

shinigami = discord.Embed(description="""*Naïve children. You're all so petulant, so mortal. Don't you realise that you don't want to play this game with me? You can't run from me, you can't hide from me, you can't defeat me yet you seem so insistent on trying. You think this is your game but I haven't had my turn. You think you hold all the cards until you realise you've got nothing but a Dead Man's Hand.*
**Actions:**
*Channel Death* - Targets one player each night. If any of that player's actions or abilities would cause any player to gain a Save that night, the player who would gain the save is instead targeted with an Attack of equal strength. If a player dies from this effect, it is stated that a Shinigami killed them, and the Shinigami gains the Save the dead player would have received.
**Objectives:**
- Have at least one Evil role survive until all Good and Neutral roles have been eliminated.
**Tags:**
- Evil
- Counteractive/Killing
- Unearthly
- Unique""",colour=0xff2323)
shinigami.set_thumbnail(url=icons["shinigami"])

glitch = discord.Embed(description="""**Actions:**
*Temporal Accelerator* - Once per game, at night, the day after this action is used is skipped, meaning that there are two nights taken consecutively. 
*Virus* - Every night, may select a player. Until that player's death, any action that player makes has a randomised target other than the Glitch. This action cannot be used until that player is dead. 
**Abilities:**
- If an even number of actions target the Glitch in any given night, the players performing said actions now each target a random other player targeting the Glitch.
- All saves the Glitch receives become Active, regardless of their previous state.
**Objectives:**
- Be the last player alive or finish the game with no living players.
**Tags:**
- Neutral
- Chaos
- Ethereal
- Unique""",colour=0xd5ff77)
glitch.set_thumbnail(url=icons["glitch"])


fate = discord.Embed(description="""**Actions:**
*Create Spinster* - Every night, may choose one player. If that player is Good, they become the Spinster and join a private channel with the Fate. After this succeeds, the action may not be used again.
*Create Inevitable* - After turning a player into the Spinster, every night, may choose one player. If that player is Evil, they become the Inevitable and join a private channel with the Fate and Spinster.
*Measure Lifespan* - Once per game, after the Spinster uses *Spin Destiny*, the Fate must choose which night (after the current night) that the Herald must die upon. If the Herald is alive at the start of the day after this night, the Spinster, Fate and Inevitable will all commit Suicide.
**Abilities:**
- While the Spinster and Inevitable are alive, gains a Lunar Powerful Save at the start of each night.
**Objectives:**
- Change a player into the Herald and have at least one member of the Witches survive until all other roles have been eliminated.
**Tags:**
- Neutral
- Chaos/Support
- Unearthly
- Factions: *Coven, Witches*
- Unique""",colour=0xe897ff)
fate.set_thumbnail(url=icons["fate"])

herald = discord.Embed(description="""**Actions:**
*Ruin* - Must choose a player to target with an Unstoppable Attack every night. If the Herald does not choose a player, then a player is randomly chosen. If a player is randomly chosen, then that player may not be the Spinster, the Fate or the Inevitable. The Herald is told which player was randomly chosen.
**Abilities:**
- Gains an Active Standard Save at the start of every day.
- Does not need to be eliminated to fulfill any other roles' Objectives, excluding roles in the Coven faction.
**Objectives:**
- Survive to see the Spinster, Fate and Inevitable die.
**Tags:**
- Neutral
- Chaos/Killing
- Arcane
- Unique
- Achievable""",colour=0xe897ff)
herald.set_thumbnail(url=icons["herald"])

inevitable = discord.Embed(description="""**Actions:**
*Catalyse Doom* - Once per game, on the night that the Fate chose using *Measure Lifespan*, the Inevitable may target any player, causing them to Commit Suicide.
**Abilities:**
- While the Spinster and Fate are alive, gains a Lunar Powerful Save at the start of each night.
**Objectives:**
- Change a player into the Herald and have at least one member of the Witches survive until all other roles have been eliminated.
**Tags:**
- Neutral
- Killing
- Unearthly
- Factions: *Coven, Witches*
- Unique
- Achievable""",colour=0xe897ff)
inevitable.set_thumbnail(url=icons["inevitable"])

spinster = discord.Embed(description="""**Actions:**
*Spin Destiny* - Once per game, if the Spinster, Fate and Inevitable are all alive or Spectres, may choose any dead player to resurrect. This player becomes a Herald.
**Abilities:**
- While the Fate and Inevitable are alive, gains a Lunar Powerful Save at the start of each night.
**Objectives:**
- Change a player into the Herald and have at least one member of the Witches survive until all other roles have been eliminated.
**Tags:**
- Neutral
- Support
- Unearthly
- Factions: *Coven, Witches*
- Unique
- Achievable""",colour=0xe897ff)
spinster.set_thumbnail(url=icons["spinster"])

hermit = discord.Embed(description="""*I remember a young man. The young man left his home in the slums of New Marais. The young man climbed mountains, saved villages, walked through warzones and saw foreign lands. The young man lived a life of passion and adventure. That young man is not me. I have his name, I have his eyes but I do not share his fire. One day, the young man died and I walked away in his place, with a goal not to travel but to find a final resting place, lay down and complete the journey. It appears I have only one destination left.*
**Actions:**
*Uncover* - Every night, chooses one player. All of that player's actions for that night fail. At the start of the next day, it is announced that the Hermit has uncovered the [their role]. The chosen player's name and alignment are not stated. From that point onwards, when the chosen player is targeted with *Investigate*, they appear to be a random role of the opposite alignment to them, unless they are Neutral in which case they will appear as their own role.
**Abilities:**
- When their objective is fulfilled, the Hermit commits Suicide. The announcement of their death in this instance will instead say that they have 'continued on their journey', and their role is revealed as per usual.
**Objectives:**
- Live to a point when all living players other than the Hermit have been targeted with *Uncover*.
**Tags:**
- Neutral
- Investigative/Counteractive
- Human
- Unique""",colour=0x00f6ff)
hermit.set_thumbnail(url=icons["hermit"])

harbinger = discord.Embed(description="""**Actions:**
*War* - Every night, may select two players. Those players target each other with an attack equal to the strength of the last save they received. If a selected player doesn't have a save, they use a Standard Attack. 
*Famine* - Every night, may choose to make all actions that would give a player a save the night this action is used automatically fail. 
*Pestilence* - Every night, may select a player. Any player that player targets with an action and any player who targets that player with an action is targeted with a Standard Attack. The Harbinger is immune to any attacks that would occur as a result of this. 
*Death* - Every night, may choose to make a random player be targeted with an Unstoppable Attack. If there are more Good roles than Evil roles, this player is a randomly selected Good player. If there are more Evil roles than Good roles, this player is a randomly selected Evil player. 
*End of Days* - All players commit suicide. This action can only be used if *War*, *Famine*, *Pestilence* and *Death* have been used at any point in this game. If any player is alive at the start of the day after this action is used, they become an Emissary. 
**Abilities:**
- Cannot use more than one action per night.
- The Harbinger's presence is announced at the start of the game. 
- If targeted with *Investigate*, the player who targeted them cannot speak the during following day. If they have any uses of *Publish*, they lose them.
- If any effect would cause their alignment to change, the effect fails and the player whose role caused the effect becomes an Emissary. 
- Cannot have any Modifiers applied to them at the start of a game. 
- If revived, by any means, they may use *End of Days* as if all conditions had been fulfilled.
**Objectives:**
- Finish the game with no living players.
**Tags:**
- Neutral
- Unearthly
- Chaos/Killing
- Faction: *Prophets*
- Unique""",colour=0xd5ff77)
harbinger.set_thumbnail(url=icons["harbinger"])

emissary = discord.Embed(description="""**Actions:**
*Relinquish* - At night, may commit suicide. The Harbinger gains a Queued Powerful Save. 
*Venerate* - Twice per game, at night, may give the Harbinger either a Queued Standard Save or an Active Powerful save. 
**Abilities:**
- Learns the identity of the Harbinger upon becoming an Emissary.
- Cannot change role, objective or alignment by any means.
- If killed by any means other than *Relinquish*, the Harbinger gains an Active Standard Save.
- If twinned whilst becoming an Emissary, the Twin also becomes an Emissary. If twinned with the Harbinger, this does not apply. 
- This role cannot be achieved by the effect of Drunk, unless there is a Harbinger present. 
- Does not need to be eliminated for the Harbinger's objective to be fulfilled.
**Objectives:**
- Have the Harbinger complete their objective.
**Tags:**
- Neutral
- Protective/Support
- Human
- Faction: *Prophets*
- Achievable""",colour=0xd5ff77)
emissary.set_thumbnail(url=icons["emissary"])

page = discord.Embed(description="""**Actions:**
*Imitate* - At night, can flip a coin. On a heads, they may use one of the Guide's actions of their choice, providing it can be used at that time. For actions with limited uses, they count their uses of an action seperately from the Guide.
**Abilities:**
- Talks to the Guide in a private channel.
- If the Page is in the game, a random other player with a Neutral role gains the Guide modifier.
- Does not need to be eliminated to fulfil any Neutral role's objectives.
- On the Guide's death, they gain the Guide's role.
**Objectives:**
- Identical to the Guide.
**Tags:**
- Neutral
- Support
- Human
- Faction: *School*
- Unique""",colour=0xffffff)
page.set_thumbnail(url=icons["page"])

guide = discord.Embed(description="""*We'd met under some... unusual circumstances, but it's my duty to teach the page my ways - after all, I'm the only one of my kind around here.*
**Abilities:**
- See Page ('*w.roles_page*').
- Can only be applied to a Neutral role.
**Tags:**
- Faction: *School*
- Modifier
- Achievable""",colour=0xffffff)
guide.set_thumbnail(url=icons["guide"])

rojinbi = discord.Embed(description="""*He was quiet. We barely knew him, just a guy who strolled into the town one day and was found the next day in their home, with a couple of pills in one hand and a bottle of gin in the other. He's dead now, I guess. I just wish we could've known him better. He always carried himself with grace, his walk commanded respect and his posture showed humility. But, behind his eyes, there was always such a fire.*
**Abilities:**
- Commits Suicide on NIGHT 1, and appears to be a random Good Human role when dead. They are given the names of three players, randomly chosen; one Good, one Evil and one Neutral (providing there is another Neutral in the game; otherwise only two names are given). At the start of DAY 3, they are resurrected. They gain the role, alignment and modifiers of one of the players whose names they were given. They also gain all the Saves that player has at the moment the Rōjinbi is resurrected. If the player has died before that point, the Rōjinbi is still resurrected, however they gain no Saves. The player is decided using the following method: If the Direwolf is alive, then Evil. Otherwise, if the Seer is alive, then Good. If neither are alive, then Neutral (unless the Rōjinbi did not receive the name of a Neutral player, in which case Good).
**Objectives:**
- None
**Tags:**
- Neutral
- Chaos
- Ethereal
- Unique""",colour=0xffffff)
rojinbi.set_thumbnail(url=icons["rojinbi"])

conduit = discord.Embed(description="""**Abilities:**
- Every night from NIGHT 2 onwards, if any of their actions involve an Attack that attack increases in strength by one level permanently, to a maximum of Unstoppable.
- At the start of DAY 3 (or the first available day if this modifier is gained after this point), it is announced that a Seer has published the Conduit's identity. Their role will appear to be a random Evil role.
- May only be applied to players with Human Killing roles.
- If the player with this modifier is a Companion or a Twin, the other member of this faction also gains this modifier.
**Tags:** 
- Modifier""",colour=0x80659a)
conduit.set_thumbnail(url=icons["conduit"])

philanthropist = discord.Embed(description="""**Actions:**
*Purify* - Once per game, at night, may choose a player. That player become a random Human role with the identical alignment to their previous role.
*Defend* - Once per game, at night, may cause all attacks targeting Human players to be weakened by one level (and to fail if they are Standard) for the duration of the night.
**Abilities:**
- At the start of every night, is told the identity of a random Human player.
- If they vote against a non-Human in a Lynching, the Attack against that player from the Lynching is Strong rather than Standard.
- Gains a Lunar Strong Save at the start of every night. This Save only affects Attacks from non-Humans.
**Objectives:**
- Have the game end with all living players as Human.
**Tags:**
- Neutral
- Counteractive/Support
- Human
- Unique""",colour=0xff9bc4)
philanthropist.set_thumbnail(url=icons["philanthropist"])

merchant = discord.Embed(description="""**Actions:**
*Discount* - Every night, may take any action from their stock and gain access to it for the rest of the game. They may also use the gained action during the day/night that they used this action. This action is however removed from their stock.
*Advertise* - Every night, may choose any action in their stock. At the start of the following day, providing this action is still present in the Merchant's stock, it is announced that this action is in the Merchant's stock.
*Check* - At any time, may check the contents of their stock.
**Abilities:**
- Cannot change alignment or objective by any means, unless they also change role. If an effect would cause the Merchant to change their alignment or objective without also changing their role, it fails.
- Has a stock of actions that begins the game empty.
- All the actions of any player to die from a Lynching are added to the Merchant's stock.
- Their presence, but not their identity, is announced at the start of the game.
- All players other than the Merchant gain the action to once per game, at night, whilst the Merchant is alive, visit the Merchant. This action cannot be used if they do not have any other actions. They are told the contents of the Merchant's stock and must choose one of their actions (which still has uses) to lose in order to gain an action of their choice from the Merchant's stock. This action may not be used on the night on which they trade it away. If one Wolf player trades in *Maul* or *Pack Offensive*, all Wolves lose the action.
- Any player who visits the Merchant when there are no actions in their stock commits suicide that night, and any player who dies on the same night that they visit the Merchant's has all their actions added to the Merchant's stock.
- If two players visit the Merchant on the same night and both want to trade for the same action, neither gets the action but both lose the action they planned to trade.
- The Merchant is told when any player visits them, their identity and what actions they lost and gained.
- The Merchant's stock is updated at the start of every day.
**Objectives:**
- Have an amount of actions in their stock that is at least the amount of players at the start of the game at any time.
**Tags:**
- Neutral
- Chaos/Support
- Human
- Unique""",colour=0xe0be00)
merchant.set_thumbnail(url=icons["merchant"])

maid = discord.Embed(description="""*When I came to this place, I thought it'd be the start of a new life for me. I came to assist my fellow man and offer my services to get by. But now? It's a city of pain, hatred and suffering; brother turning on brother to fight an enemy that has as much of a right to exist as the rest of us do. Well, I've had enough of this. I'm going to lift up my voice so that they'll put down their guns and we can all make it a better town for our children.*
**Actions:**
*Scrub* - Once per game, at night, may choose a player. If that player gains any saves during that night, they are targeted with two Standard attacks.
*Tidy Up* - Once per game, at night, may choose a player. If that player is targeted with any attack that night, regardless of if that attack is saved against, they commit Suicide.
*Spring Cleaning* - Once per game, at night, the following night, the player given the secondmost amount of votes in the Lynch the previous day commits Suicide (the Maid can choose which player in the event of any relevant ties).
**Abilities:**
- Cannot change alignment or objective by any means, unless they also change role. If an effect would cause the Maid to change their alignment or objective without also changing their role, it fails.
- If killed, all players who have a condition that would allow them to become an Achievable role in their abilities automatically have that condition count as being fulfilled. In the event that they have multiple, one is randomly chosen.
- Does not need to be eliminated to fulfill any player's objectives.
**Objectives:**
- Have every player alive at the start of DAY 4 (not including themselves) be in the Winning Players. Any player who commits Suicide after the start of DAY 4 does not count to this. If the game ends before DAY 4, the Maid loses.
**Tags:**
- Neutral
- Chaos/Killing
- Human
- Unique""",colour=0xff9bc4)
maid.set_thumbnail(url=icons["maid"])

understudy = discord.Embed(description="""*Grief - the very worst of all human emotions. Grief can turn a brave hero into a desperate villain. Grief can throw the strongest man into a deepening spiral of emotional paralysis. Grief can make you throw out all you know - your items, your friends, even the clothes on your back. Grief can change you, make you become something you were never meant to be. Small changes at first - new haircut, new outfit, new speech patterns - until you look in the mirror and see all that you lost. Grief can break a man. Grief can make a man.*
**Actions:**
*Imitate* - Once per game, may become the role of a dead player of their choice.
**Abilities:**
- Starts the game with a Queued Powerful Save which is lost when the Understudy uses *Imitate*.
**Objectives:**
- None
**Tags:**
- Neutral
- Chaos/Support
- Human
- Unique""",colour=0xffffff)
understudy.set_thumbnail(url=icons["understudy"])

speedster = discord.Embed(description="""**Actions:**
*Supercharge* - Twice per game, at night, may choose to make all their actions act as if they were at the start of the night instead of at the end, hence acting before all effects used that night. This cannot be used on two consecutive nights. The day after this action is used, the Speedster cannot speak or vote.
*Godspeed* - Once per game, at night, may choose to cause the following day to last a maximum time of five minutes, or three minutes if the gamemode is Quickfire. The day after this action is used, the Speedster cannot speak or vote.
**Tags:**
- Modifier""",colour=0x80659a)
speedster.set_thumbnail(url=icons["speedster"])