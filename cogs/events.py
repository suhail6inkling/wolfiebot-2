import traceback
import discord
from discord.ext import commands


class Events(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.welcome = bot.get_channel(510933821055696928)

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.content.startswith("w.roles_"):
            message.content = message.content.replace("w.roles_","w.roles ")
            await bot.on_message(message)

    @commands.Cog.listener()
    async def on_member_join(self, member):
        await self.welcome.send((
            f"Well hello there, {member.mention}.\n"
            "Welcome to the Werewolf game server.\n"
            f"Introduce yourself in {self.welcome.mention} and say hi to the"
            "people here!\n\n"
            "Use `w.gamerules` to learn how to play.\n"
            "Use `w.tz` to pick the timezone you live in (to see the list of"
            "timezones, use `w.timezones`)\n"
            "Use `w.pn` to pick your preferred pronouns (`w.pronouns` to see"
            "the list)"
        ))

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        await self.welcome.send(f"Well **{member}** just left :(")

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.NotOwner):
            return await ctx.send(
                "Only the owner of the bot can use this command"
            )
        if isinstance(error, discord.ext.commands.CheckFailure):
            return await ctx.send("This command is restriced to Game Masters")
        elif isinstance(error, discord.ext.commands.errors.CommandNotFound) and not ctx.message.content.startswith("w.roles_"):
            return await ctx.message.add_reaction("\u274c")
        else:
            await ctx.send("An Error has occured")
            traceback.print_exception(type(error), error, error.__traceback__)


def setup(bot):
    bot.add_cog(Events(bot))
