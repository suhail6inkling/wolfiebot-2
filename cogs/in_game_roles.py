import discord
from discord.ext import commands


class IGR(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.timezoness = [
            'CST/CDT', 'CET/CEST', 'NZST/NZDT', 'EST/EDT',
            'AEST/AEDT', 'PST/PDT', 'IST', 'ACST/ACDT', 'GMT/BST'
        ]
        self.pronounss = ["she/her", "he/him", "they/them"]

    @commands.command()
    async def pn(self, ctx, *, pronoun=None):
        if not pronoun:
            return await ctx.invoke(self.pronouns)
        y = {}
        for x in self.pronounss:
            b = x.lower().split("/")
            b.append(x.lower())
            y[x] = b
        z = {}
        for x in y:
            for a in y[x]:
                z[a] = x
        pronoun = pronoun.lower()
        if pronoun in z.keys():
            role = discord.utils.get(ctx.guild.roles, name=z[pronoun])
            if role in ctx.author.roles:
                await ctx.author.remove_roles(role)
                await ctx.send((
                    f"{ctx.author.mention}, you have been removed "
                    f"from the {role.name} role"
                ))
            else:
                await ctx.author.add_roles(role)
                await ctx.send((
                    f"{ctx.author.mention}, you have been given "
                    f"the {role.name} role"
                ))

    @commands.command()
    async def tz(self, ctx, *, timezone=None):
        if not timezone:
            return await ctx.invoke(self.timezones)
        y = {}
        for x in self.timezoness:
            b = x.lower().split("/")
            b.append(x.lower())
            y[x] = b
        z = {}
        for x in y:
            for a in y[x]:
                z[a] = x
        timezone = timezone.lower()
        if timezone in z.keys():
            role = discord.utils.get(ctx.guild.roles, name=z[timezone])
            if role in ctx.author.roles:
                await ctx.author.remove_roles(role)
                await ctx.send((
                    f"{ctx.author.mention}, you have been removed "
                    f"from the {role.name} role"
                ))
            else:
                await ctx.author.add_roles(role)
                await ctx.send((
                    f"{ctx.author.mention}, you have been given "
                    f"the {role.name} role"
                ))

    @commands.command(hidden=False)
    async def timezones(self, ctx):
        description = "\n".join(self.timezoness)
        await ctx.send(embed=discord.Embed(
            title="Time Zones",
            description=description,
            colour=0x07b7f0
        ))

    @commands.command(hidden=False)
    async def pronouns(self, ctx):
        description = "\n".join(self.pronounss)
        await ctx.send(embed=discord.Embed(
            title="Pronouns",
            description=description,
            colour=0x07b7f0
        ))


def setup(bot):
    igr = IGR(bot)
    bot.add_cog(igr)
    dir(igr)
    print("YAY")
