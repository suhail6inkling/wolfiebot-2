from discord.ext import commands
from pytz import timezone
from datetime import datetime
from cogs.lib.checks import GMAlt


class Host(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    def is_dst(self):
        zonename = "Europe/London"
        now = datetime.now(tz=timezone(zonename))
        dst_timedelta = now.dst()

        return "BST" if dst_timedelta else "GMT"

    @commands.command(aliases=["quickfire"])
    @GMAlt
    async def qf(self, ctx, time=None):
        if not time:
            message = "NOW"
        else:
            message = f"AT {time.upper()} {self.is_dst()}"
        gr = self.bot.get_channel(510933737048113183)
        await gr.send(
            f"""@everyone QUICKFIRE GAME {message} - REACT IF PLAYING (GMed by {ctx.author.mention})"""
        )

    @commands.command()
    @GMAlt
    async def host(self, ctx, time="8pm"):
        gr = self.bot.get_channel(510933737048113183)
        await gr.send((
            f"@everyone Game today at {time} {self.is_dst()}, vote for game"
            " mode using emojis:\n"
            ":regional_indicator_a: --> Casual\n"
            ":regional_indicator_b: --> Ranked\n"
            ":regional_indicator_c: --> Moral Feud\n"
            ":regional_indicator_d: --> Anonymous Register\n"
            ":regional_indicator_e: --> Maelstrom\n"
            f"\n\nGMed by {ctx.author.mention}"
        ))


def setup(bot):
    bot.add_cog(Host(bot))
