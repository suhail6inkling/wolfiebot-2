import discord
from discord.ext import commands
from cogs.lib.checks import GM
from cogs.lib import channels
import asyncio


class InGame(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.Day = False

    @commands.command(aliases=["end","eg"])
    @GM
    async def endgame(self, ctx):
        self.Day = False
        guild = ctx.message.guild
        player_role = discord.utils.get(guild.roles, name="Player")
        dead_role = discord.utils.get(guild.roles, name="Dead")
        mayor_role = discord.utils.get(guild.roles, name="Mayor")
        deputy_role = discord.utils.get(guild.roles, name="Deputy")
        spectator_role = discord.utils.get(guild.roles, name="Spectator")
        privchannels = discord.utils.get(
            guild.categories, name="Priv Channels"
        )
        roles = [
            player_role, dead_role, mayor_role, deputy_role, spectator_role
        ]
        for role in roles:
            for member in role.members:
                await member.remove_roles(role)
        for c in privchannels.channels:
            await c.delete()
        game_channel = self.bot.get_channel(channels.game)
        voting_channel = self.bot.get_channel(channels.voting)
        perms = discord.PermissionOverwrite()
        perms.read_messages = True
        perms.send_messages = False
        perms.add_reactions = True
        await voting_channel.set_permissions(player_role, overwrite=perms)
        perms = discord.PermissionOverwrite()
        perms.send_messages = True
        perms.add_reactions = True
        await game_channel.set_permissions(player_role, overwrite=perms)
        await ctx.send("Game ended successfully")

    @commands.command(aliases=["pv"])
    @GM
    async def playervote(self, ctx, s=900):
        voting_channel = self.bot.get_channel(channels.voting)
        role = discord.utils.get(ctx.message.guild.roles, name="Player")
        players = [
            p.nick for p in ctx.message.guild.members
            if role in p.roles and p.nick is not None
        ]
        for name in [
            p.name for p in ctx.message.guild.members
            if role in p.roles and p.nick is None
        ]:
            players.append(name)
        n = round((len(role.members) / 2) + 0.5)
        voted = await ctx.invoke(
            self.bot.get_command("vote"),
            message=players, where=voting_channel, needed=n, time=s
        )
        return voted

    @commands.command(aliases=["mv"])
    async def mayorvote(self, ctx, s=900):
        voting_channel = self.bot.get_channel(channels.voting)
        await voting_channel.send("MAYORAL ELECTION")
        await ctx.invoke(self.bot.get_command("playervote"), s=s)
    
    @commands.command(aliases=["lv"])
    async def lynchvote(self, ctx, s=900):
        voting_channel = self.bot.get_channel(channels.voting)
        await voting_channel.send("LYNCH")
        await ctx.invoke(self.bot.get_command("playervote"), s=s)

    @commands.command(aliases=["n"])
    @GM
    async def night(self, ctx):
        if self.Day:
            self.Day = False
            return
        await ctx.send("It is already night or there is no game happening")

    @commands.command(aliases=["dt"])
    @GM
    async def daytimer(self, ctx, n: int, secs: int, *, message):
        a = "\n".join(message.split("/"))
        game_channel = self.bot.get_channel(channels.game)
        game_announcements_channel = self.bot.get_channel(channels.game_announcements)
        voting_channel = self.bot.get_channel(channels.voting)

        await game_announcements_channel.send("Day {}\n{}".format(n, a))
        await game_channel.send("Day {}".format(n))
        player = discord.utils.get(ctx.message.guild.roles, name="Player")
        perms = discord.PermissionOverwrite()
        perms.read_messages = True
        perms.send_messages = False
        perms.add_reactions = True
        await voting_channel.set_permissions(player, overwrite=perms)
        perms = discord.PermissionOverwrite()
        perms.send_messages = True
        perms.add_reactions = True
        await game_channel.set_permissions(player, overwrite=perms)
        for chan in [c for c in ctx.guild.channels if c.name == "seance"]:
            await chan.delete()
        self.Day = True
        for i in range(0, secs):
            await asyncio.sleep(1)
            if (secs-i) % 300 == 0:
                await game_channel.send(
                    "{} minutes remaining until Night!".format(
                        int((secs-i)/60)
                    )
                )
            if (secs-i) == 180:
                await game_channel.send("3 minutes remaining until Night!")
            if (secs-i) == 120:
                await game_channel.send("2 minutes remaining until Night!")
            if (secs-i) == 60:
                await game_channel.send("1 minute remaining until Night!")
            if (secs-i) == 30:
                await game_channel.send("30 seconds remaining until Night!")
            if (secs-i) == 10:
                await game_channel.send("10 seconds remaining until Night!")
            if not self.Day:
                break
        self.Day = False
        await game_channel.send("NIGHT {}".format(n))
        await game_announcements_channel.send("NIGHT {}".format(n))
        perms = discord.PermissionOverwrite()
        perms.read_messages = True
        perms.send_messages = False
        perms.add_reactions = False
        await voting_channel.set_permissions(player, overwrite=perms)
        perms = discord.PermissionOverwrite()
        perms.send_messages = False
        perms.add_reactions = True
        await game_channel.set_permissions(player, overwrite=perms)


def setup(bot):
    bot.add_cog(InGame(bot))
