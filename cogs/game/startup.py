import discord
from discord.ext import commands
from cogs.lib.checks import GM
from cogs.lib import roles
from cogs.lib import AllRoles, Alignments
from cogs.lib.gsheets import gsheets

class StartUp(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.gsheets = gsheets

    @commands.command(aliases=["gr"])
    @GM
    async def giveroles(self, ctx, *, message: str):
        notes_channel = self.bot.get_channel(510935148762628098)
        message = message.split(", ")
        x = []
        for m in message:
            m = m.split(": ")
            m[0] = m[0].lower()
            m[0] = m[0].replace(" ", "-")
            if "(" in m[1]:
                y = m[1].split(" (")
                m[1] = y[0]
                modifier = y[1]
                m.append(modifier[:-1])
            else:
                m.append("")
            chan = discord.utils.get(
                ctx.message.guild.channels, name="{}-priv".format(m[0])
            )
            m.append(chan)
            gm_role = discord.utils.get(
                ctx.message.guild.roles, name="Game Master"
            )
            bot_role = discord.utils.get(ctx.message.guild.roles, name="Bots")
            narr_role = discord.utils.get(
                ctx.message.guild.roles, name="Narrator"
            )
            user = [
                u for u in ctx.message.guild.members
                if chan.permissions_for(u).read_messages
                and gm_role not in u.roles
                and bot_role not in u.roles
                and narr_role not in u.roles
            ][0]
            m.append(user)
            x.append(m)
        PlayerInfo = {m[0]: [m[4], m[3], m[1], [m[2]]] for m in x}
        for player in PlayerInfo:
            if PlayerInfo[player][3] == [""]:
                PlayerInfo[player][3] = []
                modifier = False
            else:
                modifier = True
            if PlayerInfo[player][2] in [
                r for r in AllRoles if Alignments[r] == "G"
            ]:
                alignment = "Good"
            elif PlayerInfo[player][2] in [
                r for r in AllRoles if Alignments[r] == "E"
            ]:
                alignment = "Evil"
            elif PlayerInfo[player][2] in [
                r for r in AllRoles if Alignments[r] == "N"
            ]:
                alignment = "Neutral"
            else:
                await ctx.send(
                    "{} is not a role!".format(
                        PlayerInfo[player][2]
                    )
                )
            PlayerInfo[player].append(alignment)
            PlayerInfo[player].append("Alive")
            if modifier:
                mod = " {}".format(PlayerInfo[player][3][0])
            else:
                mod = ""
            if (
                PlayerInfo[player][4] == "Good" or
                PlayerInfo[player][4] == "Neutral"
            ):
                a = " {}".format(PlayerInfo[player][4])
            elif PlayerInfo[player][4] == "Evil":
                a = "n {}".format(PlayerInfo[player][4])
            output = "You are a{} {}{}!".format(a, PlayerInfo[player][2], mod)
            await PlayerInfo[player][1].send(output)
            name = PlayerInfo[player][2] \
                .replace(" ", "").lower().replace("ō", "o")
            await ctx.invoke(
                self.bot.get_command("roles"),
                role=name,
                where=PlayerInfo[player][1]
            )
            if modifier:
                name = PlayerInfo[player][3][0].replace(" ", "").lower()
                await ctx.invoke(
                    self.bot.get_command("roles"),
                    role=name,
                    where=PlayerInfo[player][1]
                )
        rolereport = ""
        for p in sorted(list(PlayerInfo)):
            mod = ""
            for m in PlayerInfo[p][3]:
                mod = "{} {}".format(mod, m)
            rolereport = (
                "{}{} - {} {}{}\n".format(
                    rolereport, p.title(),
                    PlayerInfo[p][4], PlayerInfo[p][2], mod
                    )
            )
        await notes_channel.send(rolereport)

    @commands.group(aliases=["privs"], invoke_without_command=True)
    @GM
    async def setprivs(self, ctx, *, message):

        players = message.split(", ")
        guild = ctx.guild
        privchannels = discord.utils.get(
            guild.categories, name="Priv Channels"
        )
        everyone_perms = discord.PermissionOverwrite(read_messages=False)
        priv_perms = discord.PermissionOverwrite(read_messages=True)
        gm_role = discord.utils.get(guild.roles, name="Game Master")
        bot_role = discord.utils.get(guild.roles, name="Bots")
        for p in players:
            p = p.split(": ")
            p[0] = await commands.MemberConverter().convert(ctx, p[0])
            overwrites = {
                guild.default_role: everyone_perms,
                discord.utils.get(guild.roles, name="Game Master"):
                priv_perms,
                p[0]: priv_perms
            }
            channame = "{}-priv".format(p[1].lower())
            if gm_role in p[0].roles:
                await ctx.send("{} is a GM.".format(p[0].mention))
                continue
            elif bot_role in p[0].roles:
                await ctx.send("{} is a bot.".format(p[0].mention))
                continue
            makepriv = True
            for c in privchannels.channels:
                if c.name == channame:
                    await ctx.send(
                        "The channel name `{}` is taken.".format(channame)
                    )
                    makepriv = False
                    break
                if "-priv" in c.name:
                    x = [
                        u for u in guild.members
                        if c.permissions_for(u).read_messages
                    ]
                    if p[0] in x:
                        await ctx.send(
                            "{} already has a priv channel."
                            .format(p[0].mention)
                        )
                        makepriv = False
                        break
            if makepriv:
                await guild.create_text_channel(
                    channame, overwrites=overwrites, category=privchannels
                )

    @setprivs.command(name="auto")
    @GM
    async def setprivs_auto(self, ctx):
        player = ctx.guild.get_role(roles.player)
        members = [m.id for m in player.members]
        
        async with self.gsheets:
            memberlist = await self.gsheets.get_list()

        string = ""
        for x in members:
            if x not in memberlist:
                await ctx.send(f"<@{x}> not on scoreboard. Please set priv manually.")
            else:
                string +=f"<@{x}>: {memberlist[x]}, "
        string = string.strip().strip(",")

        await ctx.invoke(self.setprivs, message=string)

    @commands.group(aliases=["players", "player"], invoke_without_subcommand=True)
    @GM
    async def setplayers(self, ctx):
        people = ctx.message.mentions

        class Bunch:
            def __init__(self, **kwargs):
                self.__dict__.update(kwargs)

        player = Bunch()
        player.id = roles.player
        for person in people:
            if player.id in [x.id for x in person.roles]:
                await person.remove_roles(player)
            else:
                await person.add_roles(player)

    @setplayers.command(name="auto")
    @GM
    async def setplayers_auto(self, ctx):
        channel = self.bot.get_channel(510933737048113183)
        message = (await channel.history(limit=1).flatten())[0]
        people = ([await r.users().flatten() for r in message.reactions])

        class Bunch:
            def __init__(self, **kwargs):
                self.__dict__.update(kwargs)

        player = Bunch()
        player.id = roles.player

        for x in people:
            for person in x:
                if player.id in [x.id for x in person.roles]:
                    await person.remove_roles(player)
                else:
                    await person.add_roles(player)


def setup(bot):
    bot.add_cog(StartUp(bot))
