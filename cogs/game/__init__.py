
from . import startup, chan_gen, ingame, give_roles, role_effects, host

modules = [startup, chan_gen, ingame, give_roles, role_effects, host]


def setup(bot):
    for module in modules:
        module.setup(bot)
