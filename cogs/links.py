import discord
from discord.ext import commands


class Links(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def gamerules(self, ctx):
        embed = discord.Embed(description=(
            "This link details the rules for playing Werewolf.\n"
            "If you have any questions or suggestions for improvement on the "
            "rules, contact Suhail with them. He'd be happy to help!\n"
            "(If that doesn't work, here's the link: "
            "https://bit.ly/werewolf-gamerules)"
        ))
        embed.set_author(
            name="Werewolf Party Game Rules",
            url='https://bit.ly/werewolf-gamerules',
            icon_url='https://i.imgur.com/hYA0Uqu.png'
        )
        await ctx.send(embed=embed)

    @commands.command(aliases=["sb"])
    async def scoreboard(self, ctx):
        embed = discord.Embed(description=(
            "This spreadsheet includes the scoreboards and a game tracker.\n"
            "(If that doesn't work, here's the link: "
            "https://bit.ly/WerewolfScoreboard)"
        ))
        embed.set_author(
            name="Werewolf Scoreboard/Tracker",
            url='https://bit.ly/WerewolfScoreboard',
            icon_url='https://i.imgur.com/hYA0Uqu.png'
        )
        await ctx.send(embed=embed)
    
    @commands.command()
    async def suggestions(self, ctx):
        embed = discord.Embed(description=(
            "This Trello board contains the suggestions to update both"
            "the Werewolf game & the Werewolf Discord server\n"
            "(If that doesn't work, here's the link: "
            "https://bit.ly/WerewolfSuggestions)"
        ))
        embed.set_author(
            name="Werewolf Suggestion Board",
            url="https://bit.ly/WerewolfSuggestions",
            icon_url="https://i.imgur.com/hYA0Uqu.png"
        )
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Links(bot))
