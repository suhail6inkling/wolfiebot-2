# WolfieBot 2 | Werewolf Bot

Welcome to the repository for WolfieBot.

Documentation is on its way. Meanwhile, here are the steps to running your own discord bot


## Step 1 - Fork the Repository

Click the `Fork` icon in the top left corner to create your own version of that repository

## Step 2 - Clone your Repository

On your newly forked repository, press the `Clone` icon and copy (using HTTPS most of the time)

## Step 3 - Add a config.py file

Add a config file containing the `TOKEN` of your discord bot

## Step 4 - Pipenv

Run `pipenv sync` to update your dependencies and create a virtual environment

Use `pipenv run start` to start the bot